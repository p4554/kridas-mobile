import axios from 'axios';
//import { format } from "date-fns";
// import { objToFormData } from "../helper/form-data";
import envs from '../config/env';
const { BACKEND_URL } = envs;

class UserService {
	async getUser(id) {
		console.log('@dinesh:: ' + id);
		if (id !== undefined) {
			const res = await axios.get(`${BACKEND_URL}users/get/${id}`);
			if (res.data.message) {
				throw new Error(res.data.message);
			}
			return res.data.data;
		}
	}
}

export default new UserService();
