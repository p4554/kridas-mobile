import { CameraType } from 'expo-camera';
import * as ImagePicker from 'expo-image-picker';
import React, { useEffect, useRef, useState } from 'react';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';

const ProfilePhotoGallery = ({
	value = {},
	setValue,
	setOpenProfilePhotoGalleryModal,
	openProfilePhotoGalleryModal,
}: any) => {
	const [hasPermission, setHasPermission] = useState<any>(null);
	const [type, setType] = useState(CameraType.back);
	const [profileImage, setProfileImage] = useState<any>(null);
	const [profileImageData, setProfileImageData] = useState<any>(null);
	const [showGallery, setShowGallery] = useState(true);

	const cameraRef = useRef<any>(null);

	useEffect(() => {
		console.log('dinesh in photogallery');
		(async () => {
			if (Platform.OS !== 'web') {
				const permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();

				if (permissionResult.granted === false) {
					alert('Permission to access camera roll is required!');
					return;
				}
				const response: any = await pickImage();

				if (response?.imageData) {
					setProfileImage(response.uri);
					setProfileImageData(response?.imageData);
				}
				// if(response?.imageData) {
				// 	// setImage(respo)
				// 	setProfileImage(response.imageData);

				// }

				//let pickerResult = await ImagePicker.launchImageLibraryAsync();
				// console.log(pickerResult);
			}
		})();
	}, []);

	const pickImage = async () => {
		let photo = await ImagePicker.launchImageLibraryAsync({
			mediaTypes: ImagePicker.MediaTypeOptions.All,
			allowsEditing: true,
			aspect: [4, 3],
			quality: 1,
		});
		try {
			return await uploadFromURI(photo);
		} catch (error) {
			console.log(error);
		}
	};

	const uploadFromURI = async (photo: any) => {
		if (!photo.cancelled) {
			const ext = photo.uri.split('.').pop();
			const filename = photo.uri.replace(/^.*[\\\/]/, '');

			let formData = new FormData();

			const imageObject = JSON.parse(
				JSON.stringify({ uri: photo.uri, type: `image/${ext}`, fileName: filename })
			);

			formData.append('image', imageObject);
		}
	};

	if (hasPermission === null) {
		return <View />;
	}
	if (hasPermission === false) {
		return <Text>No access to camera</Text>;
	}

	const onPress = () => {
		setOpenProfilePhotoGalleryModal(false);
	};

	return (
		<>
			<Modal
				isVisible={openProfilePhotoGalleryModal}
				onSwipeComplete={() => setOpenProfilePhotoGalleryModal(false)}
				swipeDirection="down"
				style={{ margin: 0, flex: 1 }}
				onBackdropPress={() => setOpenProfilePhotoGalleryModal(false)}
				onBackButtonPress={() => setOpenProfilePhotoGalleryModal(false)}
			>
				{!showGallery ? (
					<>
						<View
							style={{
								backgroundColor: '#fff',
								flex: 1,
								flexDirection: 'column',
								width: '100%',
								height: '100%',
							}}
						>
							<View>
								<Text
									style={{
										textAlign: 'center',

										padding: 16,
										fontSize: 20,
										justifyContent: 'center',
									}}
								>
									Preview
								</Text>
								{profileImage ? (
									<View
										style={{
											padding: 16,
											flex: 1,
											flexDirection: 'row',
											justifyContent: 'center',
										}}
									>
										<Image source={{ uri: profileImage }} style={styles.profileImage} />
									</View>
								) : null}
							</View>
							<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', padding: 20 }}>
								<TouchableOpacity
									style={styles.button}
									onPress={async () => {
										setShowGallery(true);
									}}
								>
									<Text style={{ color: '#333' }}> Back to Gallery </Text>
								</TouchableOpacity>
								<TouchableOpacity
									style={styles.button}
									onPress={async () => {
										//	setShowCamera(true);
									}}
								>
									<Text style={{ color: '#333' }}> Upload Picture </Text>
								</TouchableOpacity>
							</View>
						</View>
					</>
				) : null}
			</Modal>
		</>
	);
};

export default ProfilePhotoGallery;

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},

	closeButton: { justifyContent: 'flex-end', alignItems: 'flex-end', padding: 10 },
	cameraContainer: { flex: 1, flexDirection: 'row' },
	camera: {
		flex: 1,
	},
	buttonContainer: {
		flex: 1,
		backgroundColor: 'transparent',
		flexDirection: 'row',
		margin: 20,
		//justifyContent: 'space-between',
	},
	button: {
		flex: 1,
		alignSelf: 'flex-end',
		alignItems: 'center',
	},
	text: {
		fontSize: 18,
		color: 'white',
		fontWeight: 'bold',
	},
	profileImage: {
		width: '100%',
		height: 400,
		backgroundColor: 'blue',
	},
});
