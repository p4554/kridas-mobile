import React from 'react';
import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import imagesPath from '../constants/imagesPath';
import IcClose from '../images/close.svg';

const sportsProfilePicker = ({ items = [], profileValue = {}, setProfileValue, setProfileOpenModal, profileOpenModal }: any) => {
    const renderItemView = ({ item }: any) => {
		return (
			<TouchableOpacity style={styles.itemView} onPress={() => setProfileValue(item)}>
				<Text style={{ fontSize: 18 }}>{item.profile_name}</Text> 
				{item.checked ? (
					<View style={{ paddingRight: 16 }}>
						<Image source={imagesPath.icDone} style={{ height: 20, width: 20 }} />
					</View>
				) : null}
			</TouchableOpacity>
		);
	};
  return (
        <>
			<Modal
                // animationType="slide"
				// transparent={true}
				// visible={profileOpenModal}
				// onRequestClose={() => {
				// 	//	setOpenModal(!openModal);
				// }}
				isVisible={profileOpenModal}
				onSwipeComplete={() => setProfileOpenModal(false)}
				swipeDirection="down"
				style={{ margin: 0 }}
				onBackdropPress={() => setProfileOpenModal(false)}
				onBackButtonPress={() => setProfileOpenModal(false)}
			>
				<View style={styles.container}>
					<View style={styles.pickerContainer}>
						<TouchableOpacity style={styles.closeButton} onPress={() => setProfileOpenModal(!profileOpenModal)}>
							<IcClose />
						</TouchableOpacity>
						<View style={{ flex: 1, marginTop: 50 }}>
							<FlatList data={items} renderItem={renderItemView} />
						</View>
					</View>
				</View>
			</Modal>
		</>
  )
}

export default sportsProfilePicker

const styles = StyleSheet.create({
    container: {
		flex: 1,
	},
	pickerContainer: {
		backgroundColor: '#f1f1f1',
		width: '100%',
		height: '100%',
		position: 'absolute',
		bottom: 0,
	},
	closeButton: { justifyContent: 'flex-end', alignItems: 'flex-end', padding: 10 },
	itemView: {
		flex: 1,
		height: 34,
		paddingLeft: 20,
		marginTop: 10,
		flexWrap: 'wrap',
		borderBottomColor: '#d3d3d3',
		borderBottomWidth: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
})