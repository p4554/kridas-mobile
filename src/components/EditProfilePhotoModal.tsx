import React, { useState } from 'react';
import { Image, Pressable, StyleSheet, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { Text } from 'react-native-paper';
import imagesPath from '../constants/imagesPath';
import Cam from '../images/cam.svg';
import Close from '../images/close.svg';
import Picture from '../images/picture.svg';
import ProfilePhotoCapture from './ProfilePhotoCapture';
import ProfilePhotoGallery from './ProfilePhotoGallery';

const EditProfilePhotoModal = ({ value = {}, setValue, setOpenProfilePhotoModal, openProfilePhotoModal }: any) => {
	const [openProfilePhotoCameraModal, setOpenProfilePhotoCameraModal] = useState(false);
	const [openProfilePhotoGalleryModal, setOpenProfilePhotoGalleryModal] = useState(false);

	const handleUserProfileImage = (value: any) => {
		setOpenProfilePhotoCameraModal(false);
	};

	const onPress = () => {
		setOpenProfilePhotoModal(false);
	};

	const handleOpenCamera = () => {
		setOpenProfilePhotoCameraModal(!openProfilePhotoCameraModal);
		setOpenProfilePhotoModal(false);
	};

	const handleOpenGallery = () => {
		setOpenProfilePhotoGalleryModal(!openProfilePhotoGalleryModal);
		setOpenProfilePhotoModal(false);
	};

	return (
		<>
			<Modal
				isVisible={openProfilePhotoModal}
				onSwipeComplete={() => setOpenProfilePhotoModal(false)}
				swipeDirection="down"
				style={{ margin: 0 }}
				onBackdropPress={() => setOpenProfilePhotoModal(false)}
				onBackButtonPress={() => setOpenProfilePhotoModal(false)}
			>
				<Pressable style={styles.container} onPress={onPress}>
					<View style={styles.pickerContainer}>
						<View style={{ flexDirection: 'row', justifyContent: 'center' }}>
							<Image source={imagesPath.icHorizontalLine} style={{ height: 20, width: 70 }} />
						</View>

						<View style={{ padding: 10 }}>
							<View style={{ flex: 1, flexDirection: 'row', padding: 8 }}>
								<TouchableOpacity style={styles.cam} onPress={() => handleOpenCamera()}>
									<Cam />
								</TouchableOpacity>
								<TouchableOpacity onPress={() => handleOpenCamera()}>
									<Text style={{ paddingLeft: 5, fontSize: 18 }}> Take a photo </Text>
								</TouchableOpacity>
							</View>
							<View style={{ flex: 1, flexDirection: 'row', padding: 8 }}>
								<TouchableOpacity style={styles.cam} onPress={() => handleOpenGallery()}>
									<Picture />
								</TouchableOpacity>
								<TouchableOpacity onPress={() => handleOpenGallery()}>
									<Text style={{ paddingLeft: 5, fontSize: 18 }}> Open Gallery</Text>
								</TouchableOpacity>
							</View>

							<View style={{ flex: 1, flexDirection: 'row', padding: 8 }}>
								<TouchableOpacity style={styles.cam}>
									<Close />
								</TouchableOpacity>
								<Text style={{ paddingLeft: 5, fontSize: 18 }}> Remove Current Photo</Text>
							</View>
						</View>
					</View>
				</Pressable>
			</Modal>
			{openProfilePhotoCameraModal ? (
				<ProfilePhotoCapture
					setOpenProfilePhotoCameraModal={setOpenProfilePhotoCameraModal}
					openProfilePhotoCameraModal={openProfilePhotoCameraModal}
					value={value}
					setValue={setValue}
				/>
			) : null}

			{openProfilePhotoGalleryModal ? (
				<ProfilePhotoGallery
					setOpenProfilePhotoGalleryModal={setOpenProfilePhotoGalleryModal}
					openProfilePhotoGalleryModal={openProfilePhotoGalleryModal}
					value={value}
					setValue={setValue}
				/>
			) : null}
		</>
	);
};

export default EditProfilePhotoModal;

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	pickerContainer: {
		backgroundColor: '#f1f1f1',
		width: '100%',
		maxHeight: '100%',
		position: 'absolute',
		bottom: 0,
	},
	closeButton: { justifyContent: 'flex-end', alignItems: 'flex-end', padding: 10 },
	itemView: {
		flex: 1,
		height: 34,
		paddingLeft: 20,
		marginTop: 10,
		flexWrap: 'wrap',
		borderBottomColor: '#d3d3d3',
		borderBottomWidth: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	camera: {
		flex: 1,
	},
	buttonContainer: {
		flex: 1,
		backgroundColor: 'transparent',
		flexDirection: 'row',
		margin: 20,
	},
	button: {
		flex: 0.1,
		alignSelf: 'flex-end',
		alignItems: 'center',
	},
	text: {
		fontSize: 18,
		color: 'white',
	},
	cam: {
		backgroundColor: '#FFFFFF',
		padding: 5,
		borderRadius: 90,
		alignItems: 'center',
		width: 30,
		height: 30,
	},
});
