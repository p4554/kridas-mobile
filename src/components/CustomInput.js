import { Controller } from 'react-hook-form';
import { Dimensions, StyleSheet, TextInput, View } from 'react-native';

export default function CustomInput({ control, name, placeholder, secureTextEntry }) {
	return (
		<View style={styles.container}>
			<Controller
				control={control}
				name={name}
				rules={{
					required: true,
				}}
				render={({ field: { onChange, onBlur, value } }) => (
					<TextInput
						style={styles.inputStyle}
						placeholder={placeholder}
						onBlur={onBlur}
						onChangeText={onChange}
						value={value}
						secureTextEntry={secureTextEntry}
					/>
				)}
			/>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		marginTop: Dimensions.get('window').height > 600 ? 20 : 16,
		flex: 1,
	},

	inputStyle: {
		height: Dimensions.get('window').height / 22,
		width: '80%',
		marginBottom: 10,
		borderBottomColor: '#0062BD',
		borderBottomWidth: 2,
	},
});

{
	/* <Controller 
                    control={control}
                    name="First Name"
                    rules={{
                        required: true,
                    }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.inputStyle}
                            keyboardType='default'
                            placeholder='First Name'
                            onBlur={onBlur}
                            onChangeText={value => onChange(value)}
                            value={value}
                        />
                    )}
                /> */
}
