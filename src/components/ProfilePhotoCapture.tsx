import { Camera, CameraType } from 'expo-camera';
import React, { useEffect, useRef, useState } from 'react';
import { Image, Pressable, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';

const ProfilePhotoCapture = ({
	value = {},
	setValue,
	setOpenProfilePhotoCameraModal,
	openProfilePhotoCameraModal,
}: any) => {
	const [hasPermission, setHasPermission] = useState<any>(null);
	const [type, setType] = useState(CameraType.back);
	const [profileImage, setProfileImage] = useState<any>(null);
	const [profileImageData, setProfileImageData] = useState<any>(null);
	const [showCamera, setShowCamera] = useState(true);

	const cameraRef = useRef<any>(null);

	useEffect(() => {
		console.log('in photocapture');
		setShowCamera(true);
		(async () => {
			const { status } = await Camera.requestCameraPermissionsAsync();
			setHasPermission(status === 'granted');
		})();
	}, []);

	if (hasPermission === null) {
		return <View />;
	}
	if (hasPermission === false) {
		return <Text>No access to camera</Text>;
	}

	const onPress = () => {
		setOpenProfilePhotoCameraModal(false);
	};

	const takePhoto = async () => {
		if (cameraRef) {
			console.log('in take picture');
			try {
				let photo = await cameraRef.current.takePictureAsync({
					allowEditing: true,
					aspect: [4, 3],
					quality: 1,
				});

				if (!photo.cancelled) {
					const ext = photo.uri.split('.').pop();
					const filename = photo.uri.replace(/^.*[\\\/]/, '');

					let formData = new FormData();

					const imageObject = JSON.parse(
						JSON.stringify({ uri: photo.uri, type: `image/${ext}`, fileName: filename })
					);

					formData.append('image', imageObject);
				}

				return photo;
			} catch (e) {
				console.log(e);
			}
		}
	};

	return (
		<>
			<Modal
				isVisible={openProfilePhotoCameraModal}
				onSwipeComplete={() => setOpenProfilePhotoCameraModal(false)}
				swipeDirection="down"
				style={{ margin: 0, flex: 1 }}
				onBackdropPress={() => setOpenProfilePhotoCameraModal(false)}
				onBackButtonPress={() => setOpenProfilePhotoCameraModal(false)}
			>
				<Pressable style={styles.container} onPress={onPress}>
					<View style={styles.cameraContainer}>
						{showCamera ? (
							<Camera style={styles.camera} type={type} ratio={'4:3'} ref={cameraRef}>
								<View style={styles.buttonContainer}>
									<TouchableOpacity
										style={styles.button}
										onPress={() => {
											setType(type === CameraType.back ? CameraType.front : CameraType.back);
										}}
									>
										<Text style={styles.text}> Flip </Text>
									</TouchableOpacity>

									<TouchableOpacity
										style={styles.button}
										onPress={async () => {
											const photo = await takePhoto();
											//Alert.alert('Photo', 'Photo taken', [{ text: 'OK' }]);

											if (!photo.cancelled) {
												setProfileImage(photo.uri);
												setProfileImageData(photo?.imageData);
											}
											setShowCamera(false);
											//	setOpenProfilePhotoCameraModal(false);
										}}
									>
										<Text style={styles.text}> Photo </Text>
									</TouchableOpacity>

									<TouchableOpacity
										style={styles.button}
										onPress={async () => {
											setOpenProfilePhotoCameraModal(false);
										}}
									>
										<Text style={styles.text}> Cancel </Text>
									</TouchableOpacity>
								</View>
							</Camera>
						) : (
							<>
								<View
									style={{
										backgroundColor: '#fff',
										flex: 1,
										flexDirection: 'column',
										width: '100%',
										height: '100%',
									}}
								>
									<View>
										<Text
											style={{
												textAlign: 'center',

												padding: 16,
												fontSize: 20,
												justifyContent: 'center',
											}}
										>
											Preview
										</Text>
										{profileImage ? (
											<View
												style={{
													padding: 16,
													flex: 1,
													flexDirection: 'row',
													justifyContent: 'center',
												}}
											>
												<Image source={{ uri: profileImage }} style={styles.profileImage} />
											</View>
										) : null}
									</View>
									<View
										style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', padding: 20 }}
									>
										<TouchableOpacity
											style={styles.button}
											onPress={async () => {
												setShowCamera(true);
											}}
										>
											<Text style={{ color: '#333' }}> Back to Camera </Text>
										</TouchableOpacity>
										<TouchableOpacity
											style={styles.button}
											onPress={async () => {
												//	setShowCamera(true);
											}}
										>
											<Text style={{ color: '#333' }}> Upload Picture </Text>
										</TouchableOpacity>
									</View>
								</View>
							</>
						)}
					</View>
				</Pressable>
			</Modal>
		</>
	);
};

export default ProfilePhotoCapture;

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},

	closeButton: { justifyContent: 'flex-end', alignItems: 'flex-end', padding: 10 },
	cameraContainer: { flex: 1, flexDirection: 'row' },
	camera: {
		flex: 1,
	},
	buttonContainer: {
		flex: 1,
		backgroundColor: 'transparent',
		flexDirection: 'row',
		margin: 20,
		//justifyContent: 'space-between',
	},
	button: {
		flex: 1,
		alignSelf: 'flex-end',
		alignItems: 'center',
	},
	text: {
		fontSize: 18,
		color: 'white',
		fontWeight: 'bold',
	},
	profileImage: {
		width: '100%',
		height: 400,
		backgroundColor: 'blue',
	},
});
