import React, { useState } from 'react';
// import {
//   Checkbox,
//   FormControl,
//   FormControlLabel,
//   FormLabel,
// } from "@material-ui/core";
import { Controller } from 'react-hook-form';
import { Checkbox } from 'react-native-paper';
import { FormInputProps } from './FormInputProps';

const options = [
	{
		label: 'Checkbox Option 1',
		value: '1',
	},
	{
		label: 'Checkbox Option 2',
		value: '2',
	},
];

export const FormInputMultiCheckbox: React.FC<FormInputProps> = ({
	name,
	control,
	// setValue,
	label,
}) => {
	const [selectedItems, setSelectedItems] = useState<any>([]);

	const handleSelect = (value: any) => {
		const isPresent = selectedItems.indexOf(value);
		if (isPresent !== -1) {
			const remaining = selectedItems.filter((item: any) => item !== value);
			setSelectedItems(remaining);
		} else {
			setSelectedItems((prevItems: any) => [...prevItems, value]);
		}
	};

	// useEffect(() => {
	//   setValue(name, selectedItems);
	// }, [selectedItems]);

	return (
		<div>
			{options.map((option: any) => {
				return (
					<Controller
						name={name}
						render={({}) => {
							return (
								<Checkbox
									status={option.checked ? 'checked' : 'unchecked'}
									// onPress={() => {
									//   setChecked(!option.checked);
									// }}
								/>
							);
						}}
						control={control}
					/>
				);
			})}
		</div>
	);
};
