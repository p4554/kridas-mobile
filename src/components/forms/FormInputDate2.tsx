import DateTimePicker from '@react-native-community/datetimepicker';
import React from 'react';
import { Controller } from 'react-hook-form';
import { View } from 'react-native';
import { HelperText, TextInput } from 'react-native-paper';
import { FormInputProps } from './FormInputProps';

export const FormInputDate2 = ({
	name,
	control,
	defaultValue,
	label,
	placeHolder,
	onCustomChange,
	onHandleShowDatePicker,
	showDatePicker2,
	dtValue,
	dateMode,
	rules,
}: FormInputProps) => {
	return (
		<View>
			<Controller
				name={name}
				control={control}
				rules={rules}
				render={({ field: { value, onChange, onBlur }, fieldState: { error }, formState }) => (
					<>
						<View>
							<TextInput
								secureTextEntry={false}
								multiline={false}
								label={label}
								defaultValue={defaultValue}
								placeholder={placeHolder}
								keyboardType={'default'}
								autoComplete={'off'}
								right={
									<TextInput.Icon name={'calendar'} onPress={() => onHandleShowDatePicker('date')} />
								}
								left={undefined}
								value={dtValue}
								onChangeText={onCustomChange === null ? onChange : onCustomChange}
								autoCapitalize="none"
								dense={true}
								autoFocus={false}
								mode={'flat'}
								disabled={true}
								theme={{
									colors: error ? { primary: 'red' } : { primary: '#0062BD' },
								}}
							/>
						</View>

						{showDatePicker2 && (
							<DateTimePicker
								testID={name}
								value={value || new Date()}
								onChange={onCustomChange}
								mode={dateMode}
								display="default"
								maximumDate={new Date()}
							/>
						)}
						{error && <HelperText type="error">{error.message || 'Error'}</HelperText>}
					</>
				)}
			/>
		</View>
		// <MuiPickersUtilsProvider utils={DateFnsUtils}>
		//   <Controller
		//     name={name}
		//     control={control}
		//     render={({ field, fieldState, formState }) => (
		//       <KeyboardDatePicker
		//         fullWidth
		//         variant="inline"
		//         defaultValue={new Date()}
		//         id={`date-${Math.random()}`}
		//         label={label}
		//         rifmFormatter={(val) => val.replace(/[^[a-zA-Z0-9-]*$]+/gi, "")}
		//         refuse={/[^[a-zA-Z0-9-]*$]+/gi}
		//         autoOk
		//         KeyboardButtonProps={{
		//           "aria-label": "change date",
		//         }}
		//         format={DATE_FORMAT}
		//         {...field}
		//       />
		//     )}
		//   />
		// </MuiPickersUtilsProvider>
	);
};

// onPress={() => showMode('date')}
