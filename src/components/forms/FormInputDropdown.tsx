import React from 'react';
import { Controller } from 'react-hook-form';
import { FormInputProps } from './FormInputProps';

export const FormInputDropdown: React.FC<FormInputProps> = ({
	name,
	label,
	control,
	defaultValue,
	options,
	width,
	children,
	onCustomChange = null,
}) => {
	const generateSingleOptions = () => {
		return options.map((option: any) => {
			return (
				<></>
				// <MenuItem key={option.value} value={option.value}>
				// 	{option.label}
				// </MenuItem>
			);
		});
	};

	return (
		<Controller
			control={control}
			name={name}
			render={({ field: { onChange, value } }) => (
				<></>
				// <select
				// 	onChange={onCustomChange === null ? onChange : onCustomChange}
				// 	value={value}
				// 	defaultValue={defaultValue}
				// >
				// 	{/* {generateSingleOptions()} */}
				// 	{children}
				// </select>
			)}
		/>
	);
};
