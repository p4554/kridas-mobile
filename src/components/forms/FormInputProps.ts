export interface FormInputProps {
	name: string;
	secureTextEntry?: boolean;
	control?: any;
	label?: string;
	placeHolder?: string;
	autoComplete?: 'off';
	dense?: boolean;
	autoFocus?: boolean;
	defaultValue?: any;
	options?: any;
	width?: any;
	mode?: 'flat' | 'outlined' | undefined;
	children?: any;
	onCustomChange?: any;
	rules?: any;
	right?: any;
	left?: any;
	multiline?: boolean;
	keyboardType?: 'default' | 'number-pad' | 'decimal-pad' | 'numeric' | 'email-address' | 'phone-pad' | 'url';
	show?: boolean;
	onHandleShowDatePicker?: any;
	showDatePicker?: boolean;
	showDatePicker2?: boolean;
	dtValue?: any;
	dateMode?: any;
	numberOfLines?: number;
}
