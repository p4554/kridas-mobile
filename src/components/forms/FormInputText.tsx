import React from 'react';
import { Controller } from 'react-hook-form';
import { View } from 'react-native';
import { HelperText, TextInput } from 'react-native-paper';
import { FormInputProps } from './FormInputProps';

const FormInputText = (
	{
		name,
		secureTextEntry,
		control,
		label,
		placeHolder,
		autoComplete,
		dense,
		autoFocus,
		mode,
		onCustomChange = null,
		rules = {},
		right = undefined,
		left = undefined,
		multiline = false,
		numberOfLines = 1,
		keyboardType = 'default',
	}: FormInputProps,
	ref: any
) => {
	return (
		<Controller
			name={name}
			control={control}
			rules={rules}
			render={({ field: { onChange, value }, fieldState: { error }, formState }) => (
				<View>
					<TextInput
						ref={ref}
						secureTextEntry={secureTextEntry}
						multiline={multiline}
						numberOfLines={numberOfLines}
						label={label}
						placeholder={placeHolder}
						keyboardType={keyboardType}
						autoComplete={autoComplete}
						right={right}
						left={left !== undefined ? <TextInput.Icon name={left} /> : undefined}
						value={value}
						onChangeText={onCustomChange === null ? onChange : onCustomChange}
						autoCapitalize="none"
						dense={dense}
						autoFocus={autoFocus}
						mode={mode}
						theme={{
							colors: error ? { primary: 'red' } : { primary: '#0062BD' },
						}}
					/>
					{error && <HelperText type="error">{error.message || 'Error'}</HelperText>}
				</View>
			)}
		/>
	);
};

export default React.forwardRef(FormInputText);
