import { Picker } from '@react-native-picker/picker';
import React from 'react';
import { Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const CustomPicker = ({ openModal, setOpenModal, value, setValue, items, displayElement }: any) => {
	const pickerData = (data: any) => {
		return (
			data?.length > 0 &&
			data.map((val: any, index: any) => (
				<Picker.Item label={displayElement !== '' ? val[displayElement] : val} value={val} key={index} />
			))
		);
	};

	return (
		<>
			<Modal
				animationType="slide"
				transparent={true}
				visible={openModal}
				onRequestClose={() => {
					//	setModalOpen(!modalOpen);
				}}
			>
				<View style={styles.container}>
					<View style={styles.pickerContainer}>
						<TouchableOpacity style={styles.closeButton} onPress={() => setOpenModal(!openModal)}>
							<Text>Close</Text>
						</TouchableOpacity>

						<Picker selectedValue={value} onValueChange={(itemValue, itemIndex) => setValue(itemValue)}>
							{pickerData(items)}
						</Picker>
					</View>
				</View>
			</Modal>
		</>
	);
};

export default CustomPicker;

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	pickerContainer: {
		backgroundColor: '#fff',
		width: '100%',
		height: '40%',
		position: 'absolute',
		bottom: 0,
	},
	closeButton: { justifyContent: 'center', alignItems: 'center', marginTop: '10%' },
});

{
	/* <CustomPicker
setOpenModal={setOpenModal}
openModal={openModal}
value={selectedCountry}
setValue={handleSelectedCountry}
items={countryArray}
displayElement={'country_name'}
/> */
}
