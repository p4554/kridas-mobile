import React from 'react';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import imagesPath from '../constants/imagesPath';

const CustomDropDown = ({ items = [], value = {}, onSelect = () => {}, setOpenDropDown, openDropDown }: any) => {
	//	const [showOption, setShowOption] = useState(false);

	const onSelectedItem = (val: any) => {
		//		setShowOption(false);
		setOpenDropDown(false);
		onSelect(val);
	};

	return (
		<>
			{/* Second Picker style */}
			<>
				<TouchableOpacity
					style={styles.dropDownStyle}
					activeOpacity={0.8}
					// onPress={() => setShowOption(!showOption)}
					onPress={() => setOpenDropDown(!openDropDown)}
				>
					<Text> {!!value ? value?.country_name : 'Choose a Country'}</Text>
					<Image
						style={{ transform: [{ rotate: openDropDown ? '180deg' : '0deg' }] }}
						source={imagesPath.icDropDown}
					/>
				</TouchableOpacity>
				{openDropDown && (
					<View>
						<View
							style={{
								backgroundColor: 'orange',
								padding: 8,
								borderRadius: 6,
								maxHeight: 120,
								zIndex: 1,
								position: 'absolute',
								width: '100%',
							}}
						>
							<ScrollView keyboardShouldPersistTaps="handled" showsVerticalScrollIndicator={false}>
								{items?.length > 0 &&
									items.map((val: any, index: any) => {
										return (
											<TouchableOpacity
												key={String(index)}
												onPress={() => onSelectedItem(val)}
												style={{
													...styles.selectedItemStyle,
													backgroundColor:
														val.country_name === value?.country_name ? 'pink' : 'white',
												}}
											>
												<Text>{val.country_name}</Text>
											</TouchableOpacity>
										);
									})}
							</ScrollView>
						</View>
					</View>
				)}
			</>
		</>
	);
};

export default CustomDropDown;

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	pickerContainer: {
		backgroundColor: '#fff',
		width: '100%',
		height: '40%',
		position: 'absolute',
		bottom: 0,
	},
	closeButton: { justifyContent: 'center', alignItems: 'center', marginTop: '10%' },
	dropDownStyle: {
		backgroundColor: 'rgba(0,0,0,0.2)',
		padding: 8,
		borderRadius: 6,
		minHeight: 42,

		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginBottom: 9,
		zIndex: 999,
		position: 'relative',
	},
	selectedItemStyle: {
		paddingVertical: 8,
		borderRadius: 4,
		paddingHorizontal: 6,
		marginBottom: 2,
	},
});

// todo : TouchableWithoutFeedback - to close the list box when click outside of it
{
	/* <CustomDropDown
						items={countryArray}
						value={selectedItem}
						onSelect={onSelect}
						setOpenDropDown={setOpenDropDown}
						openDropDown={openDropDown}
					/> */
}
