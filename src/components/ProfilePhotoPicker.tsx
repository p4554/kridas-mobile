import { Camera, CameraType } from 'expo-camera';
import React, { useEffect, useState } from 'react';
import { Pressable, StyleSheet, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { Text } from 'react-native-paper';

const ProfilePhotoPicker = ({
	value = {},
	setValue,
	setOpenProfilePhotoCameraModal,
	openProfilePhotoCameraModal,
}: any) => {
	const [hasPermission, setHasPermission] = useState<any>(null);
	const [type, setType] = useState(CameraType.back);

	useEffect(() => {
		(async () => {
			const { status } = await Camera.requestCameraPermissionsAsync();
			setHasPermission(status === 'granted');
		})();
	}, []);

	if (hasPermission === null) {
		return <View />;
	}
	if (hasPermission === false) {
		return <Text>No access to camera</Text>;
	}

	const onPress = () => {
		setOpenProfilePhotoCameraModal(false);
	};

	return (
		<>
			<Modal
				isVisible={openProfilePhotoCameraModal}
				onSwipeComplete={() => setOpenProfilePhotoCameraModal(false)}
				swipeDirection="down"
				style={{ margin: 0, flex: 1 }}
				onBackdropPress={() => setOpenProfilePhotoCameraModal(false)}
				onBackButtonPress={() => setOpenProfilePhotoCameraModal(false)}
			>
				<Pressable style={styles.container} onPress={onPress}>
					<View style={styles.cameraContainer}>
						<Camera style={styles.camera} type={type} ratio={'4:3'}>
							<View style={styles.buttonContainer}>
								<TouchableOpacity
									style={styles.button}
									onPress={() => {
										setType(type === CameraType.back ? CameraType.front : CameraType.back);
									}}
								>
									<Text style={styles.text}> Flip </Text>
								</TouchableOpacity>
							</View>
						</Camera>
					</View>
				</Pressable>
			</Modal>
		</>
	);
};

export default ProfilePhotoPicker;

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},

	closeButton: { justifyContent: 'flex-end', alignItems: 'flex-end', padding: 10 },
	cameraContainer: { flex: 1, flexDirection: 'row' },
	camera: {
		flex: 1,
	},
	buttonContainer: {
		flex: 1,
		backgroundColor: 'transparent',
		flexDirection: 'row',
		margin: 20,
	},
	button: {
		flex: 0.1,
		alignSelf: 'flex-end',
		alignItems: 'center',
	},
	text: {
		fontSize: 18,
		color: 'white',
	},
});
