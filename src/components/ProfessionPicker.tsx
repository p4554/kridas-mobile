import React from 'react';
import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import imagesPath from '../constants/imagesPath';
import IcClose from '../images/close.svg';

const ProfessionPicker = ({ items = [], professionValue = {}, setProfessionValue, setProfessionOpenModal, professionOpenModal }: any) => {
    const renderItemView = ({ item }: any) => {
		return (
			<TouchableOpacity style={styles.itemView} onPress={() => setProfessionValue(item)}>
				<Text style={{ fontSize: 18 }}>{item.lookup_value}</Text> 
				{item.checked ? (
					<View style={{ paddingRight: 16 }}>
						<Image source={imagesPath.icDone} style={{ height: 20, width: 20 }} />
					</View>
				) : null}
			</TouchableOpacity>
		);
	};
  return (
        <>
			<Modal
				isVisible={professionOpenModal}
				onSwipeComplete={() => setProfessionOpenModal(false)}
				swipeDirection="down"
				style={{ margin: 0 }}
				onBackdropPress={() => setProfessionOpenModal(false)}
				onBackButtonPress={() => setProfessionOpenModal(false)}
			>
				<View style={styles.container}>
					<View style={styles.pickerContainer}>
						<TouchableOpacity style={styles.closeButton} onPress={() => setProfessionOpenModal(!professionOpenModal)}>
							<IcClose />
						</TouchableOpacity>
						<View style={{ flex: 1, marginTop: 50 }}>
							<FlatList data={items} renderItem={renderItemView} />
						</View>
					</View>
				</View>
			</Modal>
		</>
  )
}

export default ProfessionPicker

const styles = StyleSheet.create({
    container: {
		flex: 1,
	},
	pickerContainer: {
		backgroundColor: '#f1f1f1',
		width: '100%',
		height: '100%',
		position: 'absolute',
		bottom: 0,
	},
	closeButton: { justifyContent: 'flex-end', alignItems: 'flex-end', padding: 10 },
	itemView: {
		flex: 1,
		height: 34,
		paddingLeft: 20,
		marginTop: 10,
		flexWrap: 'wrap',
		borderBottomColor: '#d3d3d3',
		borderBottomWidth: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
})