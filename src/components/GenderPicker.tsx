import React from 'react';
import { Image, Pressable, StyleSheet, View } from 'react-native';
import Modal from 'react-native-modal';
import { RadioButton, Text } from 'react-native-paper';
import imagesPath from '../constants/imagesPath';

const GenderPicker = ({ value = {}, setValue, setOpenGenderModal, openGenderModal }: any) => {
	const onPress = () => {
		setOpenGenderModal(false);
	};

	return (
		<>
			<Modal
				// animationType="slide"
				// transparent={true}
				// presentationStyle="overFullScreen"
				// statusBarTranslucent={false}
				isVisible={openGenderModal}
				onSwipeComplete={() => setOpenGenderModal(false)}
				swipeDirection="down"
				style={{ margin: 0 }}
				onBackdropPress={() => setOpenGenderModal(false)}
				onBackButtonPress={() => setOpenGenderModal(false)}

				// onRequestClose={() => {
				// 	setOpenGenderModal(false);
				// }}
			>
				<Pressable style={styles.container} onPress={onPress}>
					<View style={styles.pickerContainer}>
						<View style={{ flexDirection: 'row', justifyContent: 'center' }}>
							<Image source={imagesPath.icHorizontalLine} style={{ height: 20, width: 70 }} />
						</View>

						<View style={{ padding: 10 }}>
							<RadioButton.Group onValueChange={(newValue) => setValue(newValue)} value={value}>
								<View
									style={{
										padding: 4,
										flexDirection: 'row',
										justifyContent: 'space-between',

										height: 50,
									}}
								>
									<Text style={{ fontSize: 18 }}>Male</Text>
									<RadioButton value="M" />
								</View>
								<View
									style={{
										padding: 4,
										flexDirection: 'row',
										justifyContent: 'space-between',
										height: 50,
									}}
								>
									<Text style={{ fontSize: 18 }}>Female</Text>
									<RadioButton value="F" />
								</View>
								<View
									style={{
										padding: 4,
										flexDirection: 'row',
										justifyContent: 'space-between',
										height: 50,
									}}
								>
									<Text style={{ fontSize: 18 }}>Others</Text>
									<RadioButton value="O" />
								</View>
							</RadioButton.Group>
						</View>
					</View>
				</Pressable>
			</Modal>
		</>
	);
};

export default GenderPicker;

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	pickerContainer: {
		backgroundColor: '#f1f1f1',
		width: '100%',
		maxHeight: '100%',
		position: 'absolute',
		bottom: 0,
	},
});
