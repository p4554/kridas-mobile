import React from 'react';
import { useFieldArray, useForm } from 'react-hook-form';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const DynamicForm = () => {
	const { register, control } = useForm();
	const { fields, append, remove } = useFieldArray({
		control, // control props comes from useForm (optional: if you are using FormContext)
		name: 'url', // unique name for your Field Array
	});
	return (
		<>
			{fields.map(({ id }, index) => {
				return (
					<>
						<View key={id} style={{ flex: 1 }}>
							<input
								placeholder="link"
								{...register(`url.${index}.name` as const, {
									required: true,
								})}
							/>
							<TouchableOpacity onPress={() => remove(index)}>
								<Text>Remove</Text>
							</TouchableOpacity>
						</View>
						<TouchableOpacity onPress={() => append({})}>
							<Text>Add another</Text>
						</TouchableOpacity>
					</>
				);
			})}
		</>
	);
};

export default DynamicForm;

const styles = StyleSheet.create({});
