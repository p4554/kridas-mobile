import { Ionicons } from '@expo/vector-icons';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import React from 'react';
import FeedScreen from '../screens/FeedScreen';
import HomeScreen from '../screens/HomeScreen';
import MenuScreen from '../screens/MenuScreen';
import PageScreen from '../screens/PageScreen';
import EditNavigation from './EditNavigation';

const Tab = createMaterialTopTabNavigator();

const TabNavigation = () => {
	return (
		<Tab.Navigator
			style={{ paddingTop: 50 }}
			screenOptions={({ route }) => ({
				title: '',
				tabBarStyle: {
					height: 50,
				},
				tabBarIcon: ({ color, focused }) => {
					if (route.name === 'Home') {
						return <Ionicons name="home" size={25} color={focused ? '#0062BD' : 'black'} />;
					} else if (route.name === 'My Profile') {
						return (
							<Ionicons name="md-person-circle-outline" size={25} color={focused ? '#0062BD' : 'black'} />
						);
					} else if (route.name === 'Feed') {
						return <Ionicons name="newspaper-outline" size={25} color={focused ? '#0062BD' : 'black'} />;
					} else if (route.name === 'Page') {
						return (
							<Ionicons name="document-text-outline" size={25} color={focused ? '#0062BD' : 'black'} />
						);
					} else if (route.name === 'Menu') {
						return <Ionicons name="menu-outline" size={25} color={focused ? '#0062BD' : 'black'} />;
					}
				},
			})}
		>
			<Tab.Screen
				name="Home"
				component={HomeScreen}
				// options={{
				//     title: ({color, focused}) => (
				//         <Ionicons
				//             size={25}
				//             name='home'
				//             color={focused ? 'blue' : 'green'}
				//         />
				//     )
				// }}
			/>
			<Tab.Screen name="Feed" component={FeedScreen} />
			<Tab.Screen name="Page" component={PageScreen} />
			<Tab.Screen name="My Profile" component={EditNavigation} />
			<Tab.Screen name="Menu" component={MenuScreen} />
		</Tab.Navigator>
	);
};

export default TabNavigation;
