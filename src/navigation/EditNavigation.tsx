import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import BioEditScreen from '../screens/profile/BioEditScreen';
import PMBasicInfoEdit from '../screens/profile/PMBasicInfoEdit';
import ProfileHome from '../screens/profile/ProfileHome';
import ProfileManageInfo from '../screens/profile/ProfileManageInfo';
import SportsStatEditScreen from '../screens/profile/SportsStatEditScreen';
import SocialPresenceEditScreen from '../screens/SocialPresenceEditScreen';
import SportsCareerEditScreen from '../screens/SportsCareerEditScreen';
import SportsEditScreen from '../screens/SportsEditScreen';

const Stack = createStackNavigator();

const EditNavigation = () => {
	return (
		<NavigationContainer independent={true}>
			<Stack.Navigator>
				<Stack.Screen name="ProfileHome" component={ProfileHome} options={{ headerShown: false }} />
				<Stack.Screen name="My Info" component={ProfileManageInfo} />
				<Stack.Screen name="Edit Basic Info" component={PMBasicInfoEdit} />
				<Stack.Screen name="Edit Bio" component={BioEditScreen} />
				<Stack.Screen name="Edit Social Presence" component={SocialPresenceEditScreen} />
				<Stack.Screen name="Edit Sports Career" component={SportsCareerEditScreen} />
				<Stack.Screen name="Edit Sports Statistics" component={SportsStatEditScreen} />
				<Stack.Screen name="Edit Interests" component={SportsEditScreen} />
			</Stack.Navigator>
		</NavigationContainer>
	);
};

export default EditNavigation;
