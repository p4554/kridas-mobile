import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import TabCenterScreen from '../screens/TabCenterScreen';
import TabOneScreen from '../screens/TabOneScreen';
import TabTwoScreen from '../screens/TabTwoScreen';

const Tab = createBottomTabNavigator();

const CustomTabBarButton = ({ children, onPress }: any) => (
	<TouchableOpacity
		style={{
			top: -30,
			justifyContent: 'center',
			alignItems: 'center',
		}}
		onPress={onPress}
	>
		<View style={{ width: 70, height: 70, borderRadius: 35, backgroundColor: '#e32f45' }}>{children}</View>
	</TouchableOpacity>
);

const Tabs = () => {
	return (
		<>
			<Tab.Navigator
				screenOptions={{
					tabBarShowLabel: false,
					tabBarStyle: {
						position: 'absolute',
						bottom: 25,
						left: 20,
						right: 20,

						backgroundColor: '#ffffff',
						borderRadius: 15,
						height: 90,
						...styles.shadow,
					},
				}}
			>
				<Tab.Screen
					name="one"
					component={TabOneScreen}
					options={{
						tabBarIcon: ({ focused }) => (
							<View style={{ alignItems: 'center', justifyContent: 'center', top: 10 }}>
								<Image
									source={require('../../assets/images/home.png')}
									resizeMode="contain"
									style={{ width: 25, height: 25, tintColor: focused ? '#e32f45' : '#748c94' }}
								/>
								<Text style={{ color: focused ? '#e32f45' : '#748c94', fontSize: 12 }}>Home</Text>
							</View>
						),
					}}
				/>

				<Tab.Screen
					name="center"
					component={TabCenterScreen}
					options={{
						tabBarIcon: ({ focused }) => (
							<Image
								source={require('../../assets/images/plus.png')}
								resizeMode="contain"
								style={{ width: 30, height: 30, tintColor: '#fff' }}
							/>
						),
						tabBarButton: (props) => <CustomTabBarButton {...props} />,
					}}
				/>

				<Tab.Screen
					name="two"
					component={TabTwoScreen}
					options={{
						tabBarIcon: ({ focused }) => (
							<View style={{ alignItems: 'center', justifyContent: 'center', top: 10 }}>
								<Image
									source={require('../../assets/images/settings.png')}
									resizeMode="contain"
									style={{ width: 25, height: 25, tintColor: focused ? '#e32f45' : '#748c94' }}
								/>
								<Text style={{ color: focused ? '#e32f45' : '#748c94', fontSize: 12 }}>Settings</Text>
							</View>
						),
					}}
				/>
			</Tab.Navigator>
		</>
	);
};

const styles = StyleSheet.create({
	shadow: {
		shadowColor: '#07F5DF0',
		shadowOffset: {
			width: 0,
			height: 10,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.5,
		elevation: 5,
	},
});

export default Tabs;
