export default {
	icDropDown: require('../../assets/images/down-arrow.png'),
	icDone: require('../../assets/images/done.png'),
	icHorizontalLine: require('../../assets/images/horizontal-line-96.png'),
};
