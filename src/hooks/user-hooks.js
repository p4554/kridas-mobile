import { useQuery } from 'react-query';
import userService from '../services/user-service';

const useUser = (userId) => {
	return useQuery(['user', userId], () => userService.getUser(userId), {
		select: (data) => {
			data['full_name'] = `${data['first_name']} ${data['last_name']}`;
			return data;
		},
		enabled: !(userId === undefined),
		// refetchOnWindowFocus: false,
		refetchOnMount: false,
	});
};

export { useUser };
