import axios from 'axios';
import React from 'react';
import { useForm } from 'react-hook-form';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import FormInputText from '../components/forms/FormInputText';
import envs from '../config/env';
import Logo from '../images/logo.svg';

function OtpVerificationScreen({ route, navigation }: { route: any; navigation: any }) {
	const { BACKEND_URL } = envs;
	const { email, firstName, lastName } = route.params;

	const [visible, setVisible] = React.useState(false);
	const onToggleSnackBar = () => setVisible(!visible);

	const onDismissSnackBar = () => setVisible(false);

	const {
		control,
		handleSubmit,
		formState: { errors },
	} = useForm();

	const onLoginInPressed = async (data: any) => {
		let payload = { email, token: data.otp };
		console.log(JSON.stringify(payload));
		let result = await otpCheck(payload);
	};

	const otpCheck = async (payload: any) => {
		await axios
			.put(`${BACKEND_URL}users/activate`, payload)
			.then(async (response) => {
				let result = response.data;
				console.log(result);
				const value = result.user_id;
				console.log(value);
				if (value !== null) {
					navigation.navigate('SuccessScreen');
				}
			})
			.catch((err) => {
				// Handle error
				console.log(err);
				onToggleSnackBar();
				// implement error handling, throw snack bar to user
			});
	};

	return (
		<View style={styles.container}>
			<View style={styles.positionLogo}>
				<Logo />
			</View>
			<Text style={styles.userName}>
				Hello {firstName} {lastName}
			</Text>
			<Text style={styles.info}>A verification code has been sent to</Text>
			<Text style={styles.email}>{email}</Text>
			<View style={styles.input}>
				<View>
					<FormInputText
						name="otp"
						secureTextEntry={false}
						control={control}
						label="otp"
						placeHolder="Enter the six digit otp code"
						autoComplete="off"
						dense={true}
						autoFocus={true}
						mode="flat"
						onCustomChange={null}
						rules={{
							required: 'OTP is required',
						}}
						keyboardType={'number-pad'}
					/>
				</View>
			</View>
			<View style={styles.button}>
				<TouchableOpacity onPress={handleSubmit(onLoginInPressed)}>
					<View style={styles.buttonStyle}>
						<Text style={styles.buttonText}>OK, Continue</Text>
					</View>
				</TouchableOpacity>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: '5%',
	},

	positionLogo: {
		alignItems: 'center',
		padding: 10,
		paddingTop: 20,
	},
	userName: {
		fontWeight: 'bold',
		fontSize: 20,
		lineHeight: 24,
		paddingTop: 25,
	},
	info: {
		paddingTop: 15,

		fontWeight: '600',
		fontSize: 16,
		lineHeight: 20,
	},

	email: {
		fontWeight: '400',
		fontSize: 20,
		color: '#A27E00',
	},
	input: {
		width: '100%',
		alignSelf: 'center',
		paddingTop: 30,
	},
	button: {
		paddingTop: 30,
		// marginLeft: Dimensions.get('window').width / 6
		alignSelf: 'center',
		width: '100%',
	},

	buttonStyle: {
		borderRadius: 2,
		paddingVertical: 14,
		// paddingHorizontal: 10,
		backgroundColor: '#0062BD',
		// width: "80%"
	},

	buttonText: {
		color: 'white',
		fontSize: 18,
		lineHeight: 24,
		textAlign: 'center',
	},
});

export default OtpVerificationScreen;
