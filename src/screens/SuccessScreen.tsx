import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import SportsLogo from '../images/sportslogo.svg';

function SuccessScreen({ navigation }: { navigation: any }) {
	const onLoginInPressed = async () => {
		navigation.navigate('LoginScreen');
	};

	return (
		<View style={styles.container}>
			<View style={styles.positionSportsLogo}>
				<SportsLogo />
			</View>
			<Text style={styles.info}>Your account has been created successfully.</Text>
			<Text style={styles.content}>Please login to continue </Text>
			<View style={styles.button}>
				<TouchableOpacity onPress={onLoginInPressed}>
					<View style={styles.buttonStyle}>
						<Text style={styles.buttonText}>Log In</Text>
					</View>
				</TouchableOpacity>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: '5%',
	},
	positionSportsLogo: {
		alignItems: 'center',
		padding: 10,
		paddingTop: 50,
	},
	info: {
		paddingTop: 25,
		// marginLeft: Dimensions.get('window').width / 8,
		fontWeight: '400',
		fontSize: 16,
	},

	content: {
		paddingTop: 25,
		// marginLeft: Dimensions.get('window').width / 8,
		fontWeight: '400',
		fontSize: 13,
		// lineHeight: 2.2*vh
	},
	button: {
		paddingTop: 30,
		// marginLeft: Dimensions.get('window').width / 6
		alignSelf: 'center',
		width: '100%',
	},

	buttonStyle: {
		borderRadius: 2,
		paddingVertical: 14,
		// paddingHorizontal: 10,
		backgroundColor: '#0062BD',
		// width: "80%"
	},

	buttonText: {
		color: 'white',
		fontSize: 18,
		lineHeight: 24,
		textAlign: 'center',
	},
});

export default SuccessScreen;
