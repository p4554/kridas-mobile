import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import ProfileManageInfo from './ProfileManageInfo';

const ProfileBasicInfo = ({ userProfile }: any) => {
	const [openModal, setOpenModal] = useState(false);

	return (
		<View style={{ marginTop: 20 }}>
			<View style={styles.basicInfo}>
				<Text style={{ fontSize: 18, fontWeight: 'bold' }}>Basic Info</Text>
				<View style={styles.title}>
					<Text style={{ fontSize: 14, fontWeight: 'bold', color: '#949494', letterSpacing: 2 }}>
						INTRO & CONTACT INFO
					</Text>
				</View>
				<View>
					<View>
						<Text style={styles.subTitle}>User Name</Text>
						<Text style={styles.details}>{userProfile?.user_name}</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>Email ID</Text>
						<Text style={styles.details}>{userProfile?.user_email}</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>Date of Birth</Text>
						<Text style={styles.details}>Hidden</Text>
					</View>
				</View>
				<View style={styles.button}>
					<TouchableOpacity onPress={() => setOpenModal(!openModal)}>
						<View style={styles.buttonStyle}>
							<Text style={styles.buttonText}>Manage Your Information</Text>
						</View>
					</TouchableOpacity>
					<ProfileManageInfo setModalOpen={setOpenModal} modalOpen={openModal} userProfile={userProfile} />
				</View>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// padding: '5%',
		backgroundColor: '#E9EBEC',
		// paddingTop: 40
	},
	header: {
		paddingBottom: 20,
		backgroundColor: '#FFFFFF',
	},
	banner: {
		paddingBottom: 50,
	},
	backgroundImage: {
		width: '100%',
		height: 140,
		// height: Dimensions.get('window').height / 7
	},
	icon: {
		padding: 5,
		paddingTop: 10,
		display: 'flex',
		flexDirection: 'row-reverse',
	},
	edit: {
		backgroundColor: '#FFFFFF',
		padding: 5,
		borderRadius: 90,
		width: 25,
		height: 25,
	},
	share: {
		backgroundColor: '#FFFFFF',
		padding: 5,
		borderRadius: 90,
		width: 25,
		height: 25,
	},
	profile: {
		display: 'flex',
		alignItems: 'center',
	},
	profileImage: {
		width: 100,
		height: 100,
	},
	editImage: {
		display: 'flex',
		flexDirection: 'column',
		alignSelf: 'flex-end',
		// paddingTop: 50,
		// justifyContent: 'center'
	},
	cam: {
		backgroundColor: '#FFFFFF',
		padding: 5,
		borderRadius: 90,
		alignItems: 'center',
		width: 30,
		height: 30,
	},
	info: {
		backgroundColor: '#FFFFFF',
		// padding: "5%"
	},
	bio: {
		padding: '5%',
	},
	bioInfo: {
		display: 'flex',
		flexDirection: 'row',
	},
	infoText: {
		fontSize: 16,
		marginTop: -5,
		paddingLeft: 5,
	},
	button: {
		padding: '5%',
		// marginLeft: Dimensions.get('window').width / 6
		alignSelf: 'center',
		width: '100%',
	},

	buttonStyle: {
		borderRadius: 2,
		borderWidth: 2,
		borderColor: '#0062BD',
		paddingVertical: 14,
		// paddingHorizontal: 10,
		backgroundColor: 'white',
	},

	buttonText: {
		color: '#0062BD',
		fontSize: 18,
		lineHeight: 24,
		textAlign: 'center',
	},
	basicInfo: {
		backgroundColor: '#FFFFFF',
		padding: '5%',
	},
	title: {
		paddingTop: 10,
		paddingBottom: 10,
	},
	subTitle: {
		fontSize: 13,
		fontWeight: 'bold',
		color: '#999999',
		paddingTop: 12,
	},

	details: {
		fontSize: 16,
		fontWeight: '400',
		paddingTop: 5,
	},
	content: {
		backgroundColor: '#FFFFFF',
		paddingTop: 20,
		padding: '5%',
	},
});

export default ProfileBasicInfo;
