import React, { useEffect, useState } from 'react';
import { ImageBackground, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Avatar } from 'react-native-paper';
import EditProfilePhotoModal from '../../components/EditProfilePhotoModal';
import envs from '../../config/env';
import Cam from '../../images/cam.svg';
import Edit from '../../images/edit.svg';
import Rect from '../../images/rect.svg';
import Share from '../../images/share.svg';

const ProfileHome = ({ userProfile }: any) => {
	const [openProfilePhotoModal, setOpenProfilePhotoModal] = useState(false);
	const [userProfileImage, setUserProfileImage] = useState(userProfile.image);

	const handleUserProfileImage = (value: any) => {
		setOpenProfilePhotoModal(false);
	};

	const { BACKEND_URL } = envs;

	const [sportsName, setSportsName] = useState<any>('');
	// console.log("sportsId",sports_id)

	useEffect(() => {
		if (userProfile?.bio_details !== null && userProfile?.bio_details !== undefined) {
			(async function () {
				console.log('dinesh**& ' + userProfile.bio_details);
				const response = await fetch(`${BACKEND_URL}sports/get/${userProfile?.bio_details?.sports_id}`);
				const result = await response.json();
				console.log('dinesh** ' + JSON.stringify(result));
				const sports_name = result?.data?.sports_name;
				console.log('sports', sports_name);
				setSportsName(sports_name);
			})();
		}

		// console.log('Danish @@: calling PMBasicInfo...');
	}, []);

	return (
		<View style={styles.container}>
			<View style={styles.header}>
				<View style={styles.banner}>
					<ImageBackground style={styles.backgroundImage} source={require('../../images/bgImage.png')}>
						<View style={styles.icon}>
							<TouchableOpacity style={styles.edit}>
								<Edit />
							</TouchableOpacity>
						</View>
						<View style={styles.icon}>
							<TouchableOpacity style={styles.share}>
								<Share />
							</TouchableOpacity>
						</View>
						<View style={styles.profile}>
							{userProfile?.user_profile_img === null ? (
								<Avatar.Text
									style={styles.profileImage}
									label={userProfile?.first_name + userProfile?.last_name}
								/>
							) : (
								// <Avatar.Text style={styles.profileImage}  label={userProfile.first_name.slice(0,1) + userProfile.last_name.slice(0,1)} />
								<Avatar.Image
									style={styles.profileImage}
									size={100}
									source={{ uri: `${userProfile?.user_profile_img}` }}
								/>
							)}
							<View style={styles.editImage}>
								<TouchableOpacity
									style={styles.cam}
									onPress={() => setOpenProfilePhotoModal(!openProfilePhotoModal)}
								>
									<Cam />
								</TouchableOpacity>
							</View>
						</View>
					</ImageBackground>
				</View>
				<View style={styles.info}>
					<Text style={{ fontSize: 18, fontWeight: 'bold', alignSelf: 'center' }}>
						{userProfile?.first_name} {userProfile?.last_name}
					</Text>
					<View style={styles.bio}>
						<View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
							<View style={styles.bioInfo}>
								<Rect />
								<Text style={styles.infoText}>
									{userProfile?.bio_details === null ? '-' : userProfile?.bio_details?.profession}
								</Text>
							</View>
							<View style={styles.bioInfo}>
								<Rect />
								<Text style={styles.infoText}>
									{userProfile?.address?.city}, {userProfile?.address?.country}
								</Text>
							</View>
						</View>
						<View style={{ display: 'flex', flexDirection: 'row', paddingTop: 10 }}>
							<Rect />
							<Text style={styles.infoText}>{sportsName === '' ? '-' : sportsName}</Text>
						</View>
						<View style={{ display: 'flex', flexDirection: 'row', paddingTop: 10 }}>
							<Rect />
							<Text style={{ fontSize: 14, opacity: 0.5, marginTop: -5, paddingLeft: 5 }}>
								{userProfile?.bio_details === null ? '-' : userProfile?.bio_details?.description}
							</Text>
						</View>
					</View>
					<View style={styles.button}>
						<TouchableOpacity>
							<View style={styles.buttonStyle}>
								<Text style={styles.buttonText}>Get Verified Badge</Text>
							</View>
						</TouchableOpacity>
					</View>
				</View>
			</View>

			<EditProfilePhotoModal
				setOpenProfilePhotoModal={setOpenProfilePhotoModal}
				openProfilePhotoModal={openProfilePhotoModal}
				value={userProfileImage}
				setValue={handleUserProfileImage}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// padding: '5%',
		backgroundColor: '#E9EBEC',
		// paddingTop: 40
	},
	header: {
		paddingBottom: 20,
		backgroundColor: '#FFFFFF',
	},
	banner: {
		paddingBottom: 50,
	},
	backgroundImage: {
		width: '100%',
		height: 140,
		// height: Dimensions.get('window').height / 7
	},
	icon: {
		padding: 5,
		paddingTop: 10,
		display: 'flex',
		flexDirection: 'row-reverse',
	},
	edit: {
		backgroundColor: '#FFFFFF',
		padding: 5,
		borderRadius: 90,
		width: 25,
		height: 25,
	},
	share: {
		backgroundColor: '#FFFFFF',
		padding: 5,
		borderRadius: 90,
		width: 25,
		height: 25,
	},
	profile: {
		display: 'flex',
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center',
	},
	profileImage: {
		width: 100,
		height: 100,
		borderRadius: 90,
	},
	editImage: {
		display: 'flex',
		flexDirection: 'row',
		position: 'absolute',
		justifyContent: 'center',
		alignItems: 'center',
		paddingLeft: 100,
		paddingTop: 10,
	},
	cam: {
		backgroundColor: '#FFFFFF',
		padding: 5,
		borderRadius: 90,
		alignItems: 'center',
		width: 30,
		height: 30,
	},
	info: {
		backgroundColor: '#FFFFFF',
		// padding: "5%"
	},
	bio: {
		padding: '5%',
	},
	bioInfo: {
		display: 'flex',
		flexDirection: 'row',
	},
	infoText: {
		fontSize: 16,
		marginTop: -5,
		paddingLeft: 5,
	},
	button: {
		padding: '5%',
		// marginLeft: Dimensions.get('window').width / 6
		alignSelf: 'center',
		width: '100%',
	},

	buttonStyle: {
		borderRadius: 2,
		borderWidth: 2,
		borderColor: '#0062BD',
		paddingVertical: 14,
		// paddingHorizontal: 10,
		backgroundColor: 'white',
	},

	buttonText: {
		color: '#0062BD',
		fontSize: 18,
		lineHeight: 24,
		textAlign: 'center',
	},
	basicInfo: {
		backgroundColor: '#FFFFFF',
		padding: '5%',
	},
	title: {
		paddingTop: 10,
		paddingBottom: 10,
	},
	subTitle: {
		fontSize: 13,
		fontWeight: 'bold',
		color: '#999999',
		paddingTop: 12,
	},

	details: {
		fontSize: 16,
		fontWeight: '400',
		paddingTop: 5,
	},
	content: {
		backgroundColor: '#FFFFFF',
		paddingTop: 20,
		padding: '5%',
	},
});

export default ProfileHome;

const CameraPreview = ({ photo, retakePicture, savePhoto }: any) => {
	console.log('test', photo);
	return (
		<View
			style={{
				backgroundColor: 'transparent',
				flex: 1,
				width: '100%',
				height: '100%',
			}}
		>
			<ImageBackground
				source={{ uri: photo && photo.uri }}
				style={{
					flex: 1,
				}}
			>
				<View
					style={{
						flex: 1,
						flexDirection: 'column',
						padding: 15,
						justifyContent: 'flex-end',
					}}
				>
					<View
						style={{
							flexDirection: 'row',
							justifyContent: 'space-between',
						}}
					>
						<TouchableOpacity
							onPress={retakePicture}
							style={{
								width: 130,
								height: 40,

								alignItems: 'center',
								borderRadius: 4,
							}}
						>
							<Text
								style={{
									color: '#fff',
									fontSize: 20,
								}}
							>
								Re-take
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={savePhoto}
							style={{
								width: 130,
								height: 40,

								alignItems: 'center',
								borderRadius: 4,
							}}
						>
							<Text
								style={{
									color: '#fff',
									fontSize: 20,
								}}
							>
								save photo
							</Text>
						</TouchableOpacity>
					</View>
				</View>
			</ImageBackground>
		</View>
	);
};
