import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Edit from '../../images/edit.svg';
import SocialPresenceEditScreen from '../SocialPresenceEditScreen';

const PMSocialPresence = ({ userProfile }: any) => {
    const [openModal, setOpenModal] = useState(false);
  return (
    <>
        <View style={styles.content}>
            <View style={styles.title}>
                <Text
                    style={{
                        fontSize: 14,
                        fontWeight: 'bold',
                        color: '#949494',
                        letterSpacing: 2,
                    }}
                >
                    YOUR SOCIAL PRESENCE / LINKS
                </Text>
                <TouchableOpacity
                    style={{ paddingRight: 20 }}
                    onPress={() => setOpenModal(!openModal)}
                >
                    <Edit />
                </TouchableOpacity>
                <SocialPresenceEditScreen setModalOpen={setOpenModal} modalOpen={openModal} userProfile={userProfile}/>
            </View>
            <View>
                <View>
                    <Text style={styles.subTitle}>Social Platform</Text>
                    {/* <Text style={styles.details}>{userSocial.type}</Text> */}
                    <Text style={styles.details}>Facebook</Text>
                </View>
                <View>
                    <Text style={styles.subTitle}>Link</Text>
                    {/* <Text style={styles.details}>{userSocial.link}</Text> */}
                    <Text style={styles.details}>facebook.com</Text>
                </View>
                <View>
                    <Text style={styles.subTitle}>Description</Text>
                    {/* <Text style={styles.details}>{userSocial.desc}</Text> */}
                    <Text style={styles.details}>My social account</Text>
                </View>
            </View>
        </View>
    </>
  )
}

export default PMSocialPresence

const styles = StyleSheet.create({
    content: {
		padding: '5%',
		backgroundColor: '#FFFFFF',
	},
    title: {
		paddingTop: 10,
		paddingBottom: 10,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	subTitle: {
		fontSize: 13,
		fontWeight: 'bold',
		color: '#999999',
		paddingTop: 12,
	},

	details: {
		fontSize: 16,
		fontWeight: '400',
		paddingTop: 5,
	},

})