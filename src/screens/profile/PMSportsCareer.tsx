import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Edit from '../../images/edit.svg';
import SportsCareerEditScreen from '../SportsCareerEditScreen';
import envs from '../../config/env';

const PMSportsCareer = ({ userProfile }: any) => {
    const { BACKEND_URL } = envs;
    const [openModal, setOpenModal] = useState(false);
    const user_id = userProfile.user_id
    const [careerDetails, setCareerDetails] = useState<any>([]);

    useEffect(() => {
		(async function () {
			const response = await fetch(`${BACKEND_URL}/users/statistics/getUserById/${user_id}`);
			const result = await response.json();
			const resultData = result.data;
            const resultDetails = resultData.map((item: any) => ({
                sports_name: item?.sports_name,
                sports_career: item?.sport_career.map((career: any) => ({
                    club_name: career?.name,
                    link: career?.url,
                    from: career?.from,
                    to: career?.to,
                    profile: career?.profiles,
                    achievements: career?.achievements,
                    roles: career?.roles.map((role: any) => ({
                        role: role
                    }))
                }))
            }))
            // console.log("g", resultDetails)
            
            setCareerDetails(resultDetails);
            console.log("ddd",careerDetails);
            let s = careerDetails.map((item: any) => {
                return item?.sports_career.map((value: any) => {
                    return value?.roles.map((r: any) => {
                        return r?.role
                    })
                })
            })
            console.log("ss",s);
			// console.log("userDetails", userData)
		})();
		// console.log('dinesh@@: calling PMBasicInfo...');
	}, []);
  return (
    <>
        <View style={styles.content}>
            <View style={styles.title}>
                <Text
                    style={{
                        fontSize: 14,
                        fontWeight: 'bold',
                        color: '#949494',
                        letterSpacing: 2,
                    }}
                >
                    YOUR SPORTS CAREER
                </Text>
                <TouchableOpacity
                    style={{ paddingRight: 20 }}
                    onPress={() => setOpenModal(!openModal)}
                >
                    <Edit />
                </TouchableOpacity>
                <SportsCareerEditScreen setModalOpen={setOpenModal} modalOpen={openModal} userProfile={userProfile} />
            </View>
            <View>
                <View>
                    <Text style={styles.subTitle}>Sports</Text>
                    {/* <Text>{careerDetails.map((sports: any) => {sports?.sports_name})}</Text> */}
                    {careerDetails.map((item: any) => {
                        return <Text>{item?.sports_name}</Text>
                    })}
                    
                </View>
                {careerDetails.map((item: any) => {
                    return item?.sports_career.map((value: any) => {
                        return (
                            <>
                                <View>
                                    <Text style={styles.subTitle}>Org/Club Name</Text>
                                    <Text style={styles.details}>{value?.club_name}</Text>
                                </View>
                                <View>
                                    <Text style={styles.subTitle}>Org/Club URL </Text>
                                    <Text style={styles.details}>{value?.link}</Text>
                                </View>
                                <View>
                                    <Text style={styles.subTitle}>Profile</Text>
                                    <Text style={styles.details}>{value?.profile}</Text>
                                </View>
                                {value?.roles.map((role: any) => {
                                    return (
                                        <View>
                                            <Text style={styles.subTitle}>Role</Text>
                                            <Text style={styles.details}>{role?.role}</Text>
                                        </View>
                                    )
                                })}
                                
                                <View>
                                    <Text style={styles.subTitle}>From</Text>
                                    <Text style={styles.details}>{value?.from}</Text>
                                </View>
                                <View>
                                    <Text style={styles.subTitle}>To</Text>
                                    <Text style={styles.details}>{value?.to}</Text>
                                </View>
                                <View>
                                    <Text style={styles.subTitle}>Achievements/Milestones</Text>
                                    <Text style={styles.details}>
                                        {value.achievements}
                                    </Text>
                                </View>
                            </>
                        )
                    })
                })}
                {/* <View>
                    <Text style={styles.subTitle}>Org/Club Name</Text>
                    <Text style={styles.details}>NewZealand Stars Club</Text>
                </View> */}
                {/* <View>
                    <Text style={styles.subTitle}>Org/Club URL </Text>
                    <Text style={styles.details}>www.newzealandClub.nz</Text>
                </View> */}
                
                
                
            </View>
        </View>
    </>
  )
}

export default PMSportsCareer

const styles = StyleSheet.create({
    content: {
		padding: '5%',
		backgroundColor: '#FFFFFF',
	},
    title: {
		paddingTop: 10,
		paddingBottom: 10,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	subTitle: {
		fontSize: 13,
		fontWeight: 'bold',
		color: '#999999',
		paddingTop: 12,
	},

	details: {
		fontSize: 16,
		fontWeight: '400',
		paddingTop: 5,
	},
})