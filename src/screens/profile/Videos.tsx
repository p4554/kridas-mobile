import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const Videos = ({ userProfile }: any) => {
  return (
    <View style={{ marginTop: 20, marginBottom: 30 }}>
        <View style={styles.content}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Videos</Text>
            <View style={{ paddingTop: 15, paddingBottom: 15 }}>
                <Text style={{ fontSize: 14, alignSelf: 'center' }}>9 videos</Text>
            </View>
        </View>
    </View>
  )
}

export default Videos

const styles = StyleSheet.create({
    content: {
		backgroundColor: '#FFFFFF',
		paddingTop: 20,
		padding: '5%',
	},
})