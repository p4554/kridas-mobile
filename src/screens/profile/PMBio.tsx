import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import envs from '../../config/env';
import Edit from '../../images/edit.svg';
import BioEditScreen from './BioEditScreen';

const PMBio = ({ userProfile }: any) => {
	const { BACKEND_URL } = envs;
	const [openModal, setOpenModal] = useState(false);
	const [currentUser, setCurrentUser] = useState<any>(userProfile);
	const [sportsName, setSportsName] = useState<any>('');
	const [professionName, setProfessionName] = useState<any>('');

	useEffect(() => {
		// fetch user data from backend and set it to currentUser
		(async function () {
			const response = await fetch(`${BACKEND_URL}/users/get/${currentUser?.user_id}`);
			const userDetails = await response.json();
			setCurrentUser(userDetails.data);
			if (userProfile?.bio_details !== null && userProfile?.bio_details !== undefined) {
				(async function () {
					console.log('dinesh**& ' + userProfile.bio_details);
					const response = await fetch(`${BACKEND_URL}sports/get/${currentUser?.bio_details?.sports_id}`);
					const result = await response.json();
					// console.log('dinesh** ' + JSON.stringify(result));
					const sports_name = result?.data?.sports_name;
					// console.log('sports', sports_name);
					setSportsName(sports_name);
				})();
			}
			if (userProfile?.bio_details !== null && userProfile?.bio_details !== undefined) {
				(async function () {
					console.log('dinesh**& ' + userProfile.bio_details);
					const response = await fetch(`${BACKEND_URL}lookup-table/getByType/PRF`);
					const result = await response.json();
					let profession = result.find((item: any) => item?.lookup_key === currentUser?.bio_details?.profession);
					let professionValue = profession?.lookup_value
					setProfessionName(professionValue);
					
				})();
			}
		})();
	}, [openModal]); // when modal window closes it will call this function

	return (
		<>
			<View style={styles.content}>
				<View style={styles.title}>
					<Text
						style={{
							fontSize: 14,
							fontWeight: 'bold',
							color: '#949494',
							letterSpacing: 2,
						}}
					>
						YOUR BIO
					</Text>
					<TouchableOpacity style={{ paddingRight: 20 }} onPress={() => setOpenModal(!openModal)}>
						<Edit />
					</TouchableOpacity>
					<BioEditScreen setModalOpen={setOpenModal} modalOpen={openModal} userProfile={userProfile} currentUser={currentUser} />
				</View>
				<View>
					<View>
						<Text style={styles.subTitle}>Sport</Text>
						 <Text style={styles.details}>{currentUser?.bio_details === null ? '-' : sportsName}</Text>
						 {/* <Text style={styles.details}>{sportsName}</Text> */}
						
					</View>
					<View>
						<Text style={styles.subTitle}>You are a</Text>
						<Text style={styles.details}>{currentUser?.bio_details === null ? '-' : professionName}</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>Description</Text>
						<Text style={styles.details}>{currentUser?.bio_details === null ? '-' : currentUser?.bio_details?.description}</Text>
					</View>
				</View>
			</View>
		</>
	);
};

export default PMBio;

const styles = StyleSheet.create({
	content: {
		padding: '5%',
		backgroundColor: '#FFFFFF',
	},
	basicInfo: {
		backgroundColor: '#FFFFFF',
		// paddingLeft: 20,
		// paddingTop: 20,
		padding: '5%',
		// paddingBottom: 15
	},
	title: {
		paddingTop: 10,
		paddingBottom: 10,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	subTitle: {
		fontSize: 13,
		fontWeight: 'bold',
		color: '#999999',
		paddingTop: 12,
	},

	details: {
		fontSize: 16,
		fontWeight: '400',
		paddingTop: 5,
	},
	info: {
		backgroundColor: '#FFFFFF',
		padding: '5%',
		marginTop: 20,
		// paddingBottom: 15
	},
	heading: {
		fontSize: 16,
		fontWeight: 'bold',
		color: '#555555',
	},
	infoLast: {
		backgroundColor: '#FFFFFF',
		padding: '5%',
		marginTop: 20,
		// paddingBottom: 15,
		marginBottom: 20,
	},
	button: {
		// paddingLeft: Dimensions.get('window').width / 6,
		paddingTop: 40,
		paddingBottom: 20,
		backgroundColor: '#E9EBEC',
		display: 'flex',
		flexDirection: 'row-reverse',
		// justifyContent: "space-around"
	},

	buttonStyle: {
		borderRadius: 2,
		paddingVertical: 14,
		paddingHorizontal: 40,
		backgroundColor: 'white',
		width: '100%',
		// alignSelf: 'center',
		// borderRadius: 2,
		borderWidth: 2,
		borderColor: '#0062BD',
	},

	buttonText: {
		color: '#0062BD',
		fontSize: 16,
		lineHeight: 24,
		textAlign: 'center',
	},

	buttonStyleOne: {
		borderRadius: 2,
		paddingVertical: 14,
		paddingHorizontal: 40,
		backgroundColor: '#0062BD',
		width: '100%',
		// alignSelf: 'center',
		// borderRadius: 2,
		// borderWidth: 2,
		// borderColor: '#0062BD',
	},

	buttonTextOne: {
		color: 'white',
		fontSize: 16,
		lineHeight: 24,
		textAlign: 'center',
	},
	headerContent: {
		display: 'flex',
		flexDirection: 'row',
	},
	headerText: {
		fontSize: 18,
		fontWeight: 'bold',
		alignSelf: 'center',
		paddingLeft: 10,
	},
	backIcon: {
		paddingTop: 24,
	},
	header: {
		padding: '5%',
	},
});
