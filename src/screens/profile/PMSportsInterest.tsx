import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Edit from '../../images/edit.svg';
import SportsEditScreen from '../SportsEditScreen';

const PMSportsInterest = ({ userProfile }: any) => {
    const [openModal, setOpenModal] = useState(false);
  return (
    <>
        <View style={styles.content}>
            <View style={styles.title}>
                <Text
                    style={{
                        fontSize: 14,
                        fontWeight: 'bold',
                        color: '#949494',
                        letterSpacing: 2,
                    }}
                >
                    YOUR INTERESTS
                </Text>
                <TouchableOpacity
                    style={{ paddingRight: 20 }}
                    onPress={() => setOpenModal(!openModal)}
                >
                    <Edit />
                </TouchableOpacity>
                <SportsEditScreen setModalOpen={setOpenModal} modalOpen={openModal} />
            </View>
            <View>
                <View>
                    <Text style={{ fontSize: 14, paddingTop: 5 }}>1. Cricket</Text>
                    <Text style={{ fontSize: 14, paddingTop: 5 }}>2. Cricket</Text>
                    <Text style={{ fontSize: 14, paddingTop: 5 }}>3. Cricket</Text>
                </View>
            </View>
        </View>
    </>
  )
}

export default PMSportsInterest

const styles = StyleSheet.create({
    content: {
		padding: '5%',
		backgroundColor: '#FFFFFF',
	},
    title: {
		paddingTop: 10,
		paddingBottom: 10,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
})