import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Image, Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Divider } from 'react-native-paper';
import FormInputText from '../../components/forms/FormInputText';
import ProfessionPicker from '../../components/ProfessionPicker';
import SportsPicker from '../../components/SportsPicker';
import envs from '../../config/env';
import imagesPath from '../../constants/imagesPath';
import Back from '../../images/back.svg';

const BioEditScreen = ({ modalOpen, setModalOpen, userProfile, currentUser }: any) => {
	const navigation = useNavigation();
	const { BACKEND_URL } = envs;
	// console.log("currentUser", currentUser);

	const [user, setUser] = useState<any>('');
	const [sportsloading, setSportsLoading] = useState(false);
	const [professionloading, setProfessionLoading] = useState(false);
	const [sportsSuggestionsList, setSportsSuggestionsList] = useState(null);
	const [professionSuggestionsList, setProfessionSuggestionsList] = useState(null);
	// const [selectedSports, setSelectedSports] = useState<any>(null);
	// const [selectedProfession, setSelectedProfession] = useState<any>(null);
	const dropdownController = useRef(null);
	const [description, setDescription] = useState('');
	// const [sports, setSports] = useState('');
	// const [profession, setProfession] = useState('');

	const searchRef = useRef(null);

	const [selectedSports, setSelectedSports] = useState<any>(null);

	const [selectedItem, setSelectedItem] = useState<any>(null);

	const [sportsArray, setSportsArray] = useState<any>([]);

	const [sports, setSports] = useState<any>(null);

	const [openModal, setOpenModal] = useState(false);

	const [openDropDown, setOpenDropDown] = useState(false);

	const [errSports, setErrSports] = useState('');

	const [selectedProfession, setSelectedProfession] = useState<any>(null);

	const [professionArray, setProfessionArray] = useState<any>([]);

	const [profession, setProfession] = useState<any>(null);

	const [professionOpenModal, setProfessionOpenModal] = useState(false);

	const [professionOpenDropDown, setProfessionOpenDropDown] = useState(false);

	const [errProfession, setErrProfession] = useState('');

	const {
		setValue,
		control,
		handleSubmit,
		formState: { errors },
	} = useForm();

	useEffect(() => {
		(async function () {
			const value = await AsyncStorage.getItem('currentUser');
			// const userValue = JSON.parse(value || '{}');
			// setUser(userValue);
			// const detail = userValue.address;

			getSportsArray();
			getProfessionArray();
		})();
		setErrSports('');
		setErrProfession('');
		// {not working}
		// setSelectedSports(sportsArray.find((e: any) => e.sports_id === currentUser?.bio_details?.sports_id))
		// setSelectedProfession(professionArray.find((e: any) => e.lookup_key === currentUser?.bio_details?.profession));

		setValue('description', userProfile?.bio_details?.description);
	}, []);

	const getSportsArray = async () => {
		const response = await fetch(`${BACKEND_URL}/sports/getAll`);
		const result = await response.json();
		// console.log("r", result);

		let sports_array = result.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each sport

		setSportsArray(sports_array);
		setSelectedSports(sports_array.find((e: any) => e.sports_id === currentUser?.bio_details?.sports_id));
	};

	const handleSelectedSports = (value: any) => {
		//re - initialize the variables
		setErrSports('');
		setSelectedSports(value);

		setOpenModal(false);

		// reset all checked flag to false
		let sports_array = sportsArray.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each element

		// compare selected country with each element and set checked to true
		setSportsArray(
			sports_array.map((item: { sports_id: any; checked: boolean }) =>
				item.sports_id === value.sports_id ? { ...item, checked: !item.checked } : item
			)
		);
	};

	const getProfessionArray = async () => {
		const response = await fetch(`${BACKEND_URL}lookup-table/getByType/PRF`);
		const result = await response.json();
		// const resultValue = result;
		// console.log("r", result);

		let profession_array = result.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each category

		setProfessionArray(profession_array);
		// console.log(JSON.stringify(professionArray));
		setSelectedProfession(profession_array.find((e: any) => e.lookup_key === currentUser?.bio_details?.profession));
	};

	const handleSelectedProfession = (professionValue: any) => {
		//re - initialize the variables
		setErrProfession('');
		setSelectedProfession(professionValue);

		setProfessionOpenModal(false);

		// reset all checked flag to false
		let profession_array = professionArray.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each element

		// compare selected profession with each element and set checked to true
		setProfessionArray(
			profession_array.map((item: { lookup_key: any; checked: boolean }) =>
				item.lookup_key === professionValue.lookup_key ? { ...item, checked: !item.checked } : item
			)
		);
	};

	const updateValue = async (data: any) => {
		console.log('selectedSports', selectedSports?.sports_id);
		console.log('description', data.description);
		console.log('selectedProfession', selectedProfession?.lookup_key);
		if (selectedSports === null && selectedProfession === null) {
			setErrSports('Please select Sports');
			setErrProfession('Please select Profession');
			return false;
		} else if (selectedSports === null) {
			setErrSports('Please select Sports');
			return false;
		} else if (selectedProfession === null) {
			setErrProfession('Please select Profession');
			return false;
		}

		let formData = new FormData();

		const userType = currentUser?.user_type;
		const userTypeValue = JSON.stringify(userType);
		const sportsInterest = currentUser?.sports_interested;
		const sportsInterestValue = JSON.stringify(sportsInterest);
		const sportsValue = selectedSports?.sports_id;
		const professionValue = selectedProfession?.lookup_key;
		const address = currentUser?.address;
		const addressValue = JSON.stringify(address);

		let obj = {
			sports_id: sportsValue,
			profession: professionValue,
			description: data.description,
		};

		let details = JSON.stringify(obj);

		formData.append('first_name', currentUser.first_name);
		formData.append('last_name', currentUser.last_name);
		formData.append('user_email', currentUser.user_email);
		formData.append('user_type', userTypeValue);
		formData.append('user_dob', currentUser.user_dob);
		formData.append('user_id', currentUser.user_id);
		formData.append('sports_interested', sportsInterestValue);
		formData.append('referral_code', currentUser.referral_code);
		formData.append('user_name', currentUser.user_name);
		formData.append('user_phone', currentUser.user_phone);
		formData.append('user_gender', currentUser.user_gender);
		formData.append('address', addressValue);
		formData.append('bio_details', details);

		let requestOptions = {
			method: 'PUT',
			headers: {
				Accept: 'application/json',
			},
			body: formData,
		};
		//	console.log('dinesh### ' + JSON.stringify(formData));

		fetch(`${BACKEND_URL}users`, requestOptions)
			.then((response) => response.json())
			.then(async (result) => {
				console.log(result);
				//const userValue = JSON.stringify(result);

				setModalOpen(false);
			})
			.catch((error) => {
				console.log('error# ', error);
			});
	};

	return (
		<>
			<Modal
				animationType="slide"
				transparent={false}
				visible={modalOpen}
				onRequestClose={() => {
					//	setModalOpen(!modalOpen);
				}}
			>
				<View style={styles.header}>
					<View>
						<TouchableOpacity style={styles.headerContent} onPress={() => setModalOpen(!modalOpen)}>
							<Back style={styles.backIcon} />
							<Text style={styles.headerText}>Edit Bio</Text>
						</TouchableOpacity>
					</View>
					<View>
						<TouchableOpacity onPress={handleSubmit(updateValue)}>
							<Text style={styles.buttonText}>Save</Text>
						</TouchableOpacity>
					</View>
				</View>
				<Divider />
				<View style={styles.container}>
					<View style={{ backgroundColor: 'white' }}>
						<Text
							style={{
								fontSize: 20,
								fontWeight: 'bold',
								color: '#949494',
								letterSpacing: 3.5,
								backgroundColor: 'white',
							}}
						>
							Your Bio
						</Text>

						<View style={styles.input}>
							<View>
								<View style={styles.centeredView}></View>
								<TouchableOpacity
									style={styles.dropDownStyle}
									activeOpacity={0.8}
									onPress={() => setOpenModal(!openModal)}
								>
									<Text> {!!selectedSports ? selectedSports?.sports_name : 'Choose a Sports'}</Text>
									<Image source={imagesPath.icDropDown} />
								</TouchableOpacity>
								{errSports && errSports !== '' ? (
									<View>
										<Text style={styles.errorText}>{errSports}</Text>
									</View>
								) : null}
								<SportsPicker
									setOpenModal={setOpenModal}
									openModal={openModal}
									value={selectedSports}
									setValue={handleSelectedSports}
									items={sportsArray}
									displayElement={'sports_name'}
								/>
							</View>
							<View style={styles.elementTopPadding}>
								<View style={styles.centeredView}></View>
								<TouchableOpacity
									style={styles.dropDownStyle}
									activeOpacity={0.8}
									onPress={() => setProfessionOpenModal(!professionOpenModal)}
								>
									<Text>
										{' '}
										{!!selectedProfession
											? selectedProfession?.lookup_value
											: 'Choose a Profession'}
									</Text>
									<Image source={imagesPath.icDropDown} />
								</TouchableOpacity>
								{errProfession && errProfession !== '' ? (
									<View>
										<Text style={styles.errorText}>{errProfession}</Text>
									</View>
								) : null}
								<ProfessionPicker
									setProfessionOpenModal={setProfessionOpenModal}
									professionOpenModal={professionOpenModal}
									professionValue={selectedProfession}
									setProfessionValue={handleSelectedProfession}
									items={professionArray}
									displayElement={'lookup_value'}
								/>
							</View>
							<View style={styles.elementTopPadding}>
								<FormInputText
									name="description"
									secureTextEntry={false}
									control={control}
									label="description"
									placeHolder="description"
									autoComplete="off"
									dense={true}
									autoFocus={false}
									mode="outlined"
									onCustomChange={null}
									multiline={true}
									numberOfLines={4}
									rules={{
										required: 'Please enter the text',
									}}
								/>
							</View>
						</View>
					</View>
				</View>
			</Modal>
		</>
	);
};

export default BioEditScreen;

const styles = StyleSheet.create({
	container: {
		padding: '5%',
		backgroundColor: 'white',
		flex: 1,
	},
	textInput: {
		display: 'flex',
		flexDirection: 'row',
	},

	inputStyle: {
		// height: Dimensions.get('window').height / 24,
		width: '100%',
		marginBottom: 20,
		borderBottomColor: '#0062BD',
		borderBottomWidth: 2,
	},

	input: {
		paddingTop: 20,
		backgroundColor: 'white',
	},

	inputStyleBox: {
		width: '80%',
		height: 130,
		borderWidth: 2,
		borderColor: '#2F80ED',
		borderRadius: 2,
		//   backgroundColor: "#FFFFFF",
		textAlignVertical: 'top',
		paddingTop: 5,
		paddingLeft: 15,
		fontSize: 18,
	},
	headerContent: {
		display: 'flex',
		flexDirection: 'row',
	},
	headerText: {
		fontSize: 16,
		paddingLeft: 10,
	},
	backIcon: {
		paddingTop: 23,
	},
	header: {
		padding: '5%',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	buttonText: {
		fontSize: 16,
		color: '#2F80ED',
	},
	centeredView: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 22,
	},
	dropDownStyle: {
		backgroundColor: '#fff',
		padding: 8,

		minHeight: 42,

		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',

		borderBottomColor: '#7a7a7a',
		borderBottomWidth: 1,
	},
	errorText: {
		color: 'red',
		fontSize: 12,
		paddingLeft: 5,
	},
<<<<<<< HEAD
	elementTopPadding: {
		paddingTop: 20,
	},
=======
>>>>>>> ccf4d86d45467e75f448934a8c993c629ccb386f
});
