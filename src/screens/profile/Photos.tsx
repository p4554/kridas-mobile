import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const Photos = ({ userProfile }: any) => {
  return (
    <View style={{ marginTop: 20 }}>
        <View style={styles.content}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Photos</Text>
            <View style={{ paddingTop: 15, paddingBottom: 15 }}>
                <Text style={{ fontSize: 14, alignSelf: 'center' }}>9 photos</Text>
            </View>
        </View>
    </View>
  )
}

export default Photos;

const styles = StyleSheet.create({
    content: {
		backgroundColor: '#FFFFFF',
		paddingTop: 20,
		padding: '5%',
	},
})