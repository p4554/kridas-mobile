import React from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { List } from 'react-native-paper';
import Back from '../../images/back.svg';
import PMBasicInfo from './PMBasicInfo';
import PMBio from './PMBio';
import PMSocialPresence from './PMSocialPresence';
import PMSportsCareer from './PMSportsCareer';
import PMSportsInterest from './PMSportsInterest';
import PMSportsStat from './PMSportsStat';

const ProfileManageInfo = ({ modalOpen, setModalOpen, userProfile }: any) => {
	return (
		<>
			<Modal
				isVisible={modalOpen}
				style={{ margin: 0, backgroundColor: '#fff' }}
				onBackdropPress={() => setModalOpen(false)}
				onBackButtonPress={() => setModalOpen(false)}
				backdropOpacity={1}
				backdropColor="#fff"
				hideModalContentWhileAnimating={true}
			>
				<ScrollView>
					<View style={styles.container}>
						<View style={styles.header}>
							<View>
								<TouchableOpacity style={styles.headerContent} onPress={() => setModalOpen(!modalOpen)}>
									<Back style={styles.backIcon} />
									<Text style={styles.headerText}>MANAGE YOUR INFORMATION</Text>
								</TouchableOpacity>
							</View>
						</View>
						<View>
							<List.AccordionGroup>
								<List.Accordion
									title="Basic Info"
									id="1"
									style={{ borderTopColor: '#fff', borderTopWidth: 5 }}
								>
									<PMBasicInfo userProfile={userProfile} />
								</List.Accordion>

								<List.Accordion
									title="Bio"
									id="2"
									style={{ borderTopColor: '#fff', borderTopWidth: 5 }}
								>
									<PMBio userProfile={userProfile} />
								</List.Accordion>
								<List.Accordion
									title="Social Presence"
									id="3"
									style={{ borderTopColor: '#fff', borderTopWidth: 5 }}
								>
									<PMSocialPresence userProfile={userProfile} />
								</List.Accordion>
								<List.Accordion
									title="Sports Career"
									id="4"
									style={{ borderTopColor: '#fff', borderTopWidth: 5 }}
								>
									<PMSportsCareer userProfile={userProfile} />
								</List.Accordion>
								<List.Accordion
									title="Sports Stat"
									id="5"
									style={{ borderTopColor: '#fff', borderTopWidth: 5 }}
								>
									<PMSportsStat userProfile={userProfile} />
								</List.Accordion>
								<List.Accordion
									title="Sports Selection"
									id="6"
									style={{ borderTopColor: '#fff', borderTopWidth: 5 }}
								>
									<PMSportsInterest userProfile={userProfile} />
								</List.Accordion>
							</List.AccordionGroup>
						</View>
					</View>
				</ScrollView>
			</Modal>
		</>
	);
};

export default ProfileManageInfo;

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#E9EBEC',
	},

	basicInfo: {
		backgroundColor: '#FFFFFF',
		padding: '5%',
	},
	heading: {
		fontSize: 16,
		fontWeight: 'bold',
		color: '#555555',
	},

	button: {
		paddingTop: 40,
		paddingBottom: 20,
		backgroundColor: '#E9EBEC',
		display: 'flex',
		flexDirection: 'row-reverse',
	},

	headerContent: {
		display: 'flex',
		flexDirection: 'row',
	},
	headerText: {
		fontSize: 18,
		fontWeight: 'bold',
		alignSelf: 'center',
		paddingLeft: 10,
	},
	backIcon: {
		paddingTop: 24,
	},
	header: {
		padding: '5%',
	},
});
