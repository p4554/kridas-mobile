import { format, parseISO } from 'date-fns';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import envs from '../../config/env';
import Edit from '../../images/edit.svg';
import PMBasicInfoEdit from './PMBasicInfoEdit';

const PMBasicInfo = ({ userProfile }: any) => {
	const { BACKEND_URL } = envs;
	const [openModal, setOpenModal] = useState(false);
	const [currentUser, setCurrentUser] = useState<any>(userProfile);
<<<<<<< HEAD
	const [countryData, setCountryData] = useState<any>(
		userProfile?.countryData === undefined ? undefined : JSON.parse(userProfile?.countryData)
	);
=======
	const [countryData, setCountryData] = useState<any>(JSON.parse(userProfile?.countryData));
>>>>>>> ccf4d86d45467e75f448934a8c993c629ccb386f

	useEffect(() => {
		// fetch user data from backend and set it to currentUser
		(async function () {
			const response = await fetch(`${BACKEND_URL}/users/get/${currentUser?.user_id}`);
			const userDetails = await response.json();
			setCurrentUser(userDetails.data);
<<<<<<< HEAD
			console.log('us', currentUser);
=======
			console.log("us", currentUser)
>>>>>>> ccf4d86d45467e75f448934a8c993c629ccb386f
		})();
	}, [openModal]); // when modal window closes it will call this function

	return (
		<>
			<View style={styles.content}>
				<View style={styles.title}>
					<Text
						style={{
							fontSize: 14,
							fontWeight: 'bold',
							color: '#949494',
							letterSpacing: 2,
						}}
					>
						INTRO & CONTACT INFO
					</Text>

					<TouchableOpacity style={{ paddingRight: 20 }} onPress={() => setOpenModal(!openModal)}>
						<Edit />
					</TouchableOpacity>
					{openModal && (
						<PMBasicInfoEdit setModalOpen={setOpenModal} modalOpen={openModal} currentUser={currentUser} />
					)}
				</View>
				<View>
					<View>
						<Text style={styles.subTitle}>First Name</Text>

						<Text style={styles.details}>{currentUser?.first_name}</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>Last Name</Text>
						<Text style={styles.details}>{currentUser?.last_name}</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>User Name</Text>
						<Text style={styles.details}>{currentUser?.user_name}</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>Email ID</Text>
						<Text style={styles.details}>{currentUser?.user_email}</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>Phone/Mobile Number </Text>
						<Text style={styles.details}>{currentUser?.user_phone}</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>Gender </Text>
						<Text style={styles.details}>
							{currentUser?.user_gender === 'M'
								? 'Male'
								: currentUser?.user_gender === 'F'
								? 'Female'
								: currentUser?.user_gender === 'O'
								? 'Other'
								: ''}
						</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>Date of Birth (yyyy-mm-dd)</Text>
						<Text style={styles.details}>{format(parseISO(currentUser?.user_dob), 'yyyy-MM-dd')}</Text>
					</View>

					<View>
						<Text style={styles.subTitle}>Address</Text>

						<Text style={styles.details}>{currentUser?.address?.line1}</Text>

						<Text style={styles.details}>{currentUser?.address?.line2}</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>PIN code</Text>

						<Text style={styles.details}>{currentUser?.address?.pincode}</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>Country</Text>

						<Text style={styles.details}>{countryData === undefined ? '' : countryData?.country_name}</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>State </Text>

						<Text style={styles.details}>
							{countryData === undefined
								? ''
								: countryData.country_states.find(
										(e: any) => e.state_code === currentUser?.address?.state
								  ).state_name}
						</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>City/Town</Text>

						<Text style={styles.details}>{currentUser?.address?.city}</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>Referral ID</Text>

						<Text style={styles.details}>{currentUser?.referral_code}</Text>
					</View>
				</View>
			</View>
		</>
	);
};

export default PMBasicInfo;

const styles = StyleSheet.create({
	content: {
		backgroundColor: '#FFFFFF',
		padding: '5%',
	},
	basicInfo: {
		backgroundColor: '#FFFFFF',

		padding: '5%',
	},
	title: {
		paddingTop: 10,
		paddingBottom: 10,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	subTitle: {
		fontSize: 13,
		fontWeight: 'bold',
		color: '#999999',
		paddingTop: 12,
	},

	details: {
		fontSize: 16,
		fontWeight: '400',
		paddingTop: 5,
	},
	info: {
		backgroundColor: '#FFFFFF',
		padding: '5%',
		marginTop: 20,
	},
	heading: {
		fontSize: 16,
		fontWeight: 'bold',
		color: '#555555',
	},
	infoLast: {
		backgroundColor: '#FFFFFF',
		padding: '5%',
		marginTop: 20,

		marginBottom: 20,
	},
	button: {
		paddingTop: 40,
		paddingBottom: 20,
		backgroundColor: '#E9EBEC',
		display: 'flex',
		flexDirection: 'row-reverse',
	},

	buttonStyle: {
		borderRadius: 2,
		paddingVertical: 14,
		paddingHorizontal: 40,
		backgroundColor: 'white',
		width: '100%',

		borderWidth: 2,
		borderColor: '#0062BD',
	},

	buttonText: {
		color: '#0062BD',
		fontSize: 16,
		lineHeight: 24,
		textAlign: 'center',
	},

	buttonStyleOne: {
		borderRadius: 2,
		paddingVertical: 14,
		paddingHorizontal: 40,
		backgroundColor: '#0062BD',
		width: '100%',
	},

	buttonTextOne: {
		color: 'white',
		fontSize: 16,
		lineHeight: 24,
		textAlign: 'center',
	},
	headerContent: {
		display: 'flex',
		flexDirection: 'row',
	},
	headerText: {
		fontSize: 18,
		fontWeight: 'bold',
		alignSelf: 'center',
		paddingLeft: 10,
	},
	backIcon: {
		paddingTop: 24,
	},
	header: {
		padding: '5%',
	},
});

// setSnackBarText('Profile updated successfully');
// onToggleSnackBar();
