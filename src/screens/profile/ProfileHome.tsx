import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import useAsyncStorage from '../../hooks/use-local-storage';
import { useUser } from '../../hooks/user-hooks';
import Followers from './Followers';
import Following from './Following';
import Photos from './Photos';
import ProfileBasicInfo from './ProfileBasicInfo';
import ProfileHero from './ProfileHero';
import Videos from './Videos';

const ProfileHome = ({ navigation }: { navigation: any }) => {
	const [userProfile, setUserProfile] = useState<any>('');
	const [currentUser, setCurrentUser] = useAsyncStorage('currentUser', '');

	const {
		data: userData,

		error,
	} = useUser(currentUser !== undefined ? JSON.parse(currentUser)?.user_id : undefined);

	useEffect(() => {
		if (currentUser === undefined) {
			return;
		}

		setUserProfile(userData);
	}, [currentUser]);
	return (
		<ScrollView>
			<View style={styles.container}>
				{userProfile ? (
					<>
						<ProfileHero userProfile={userProfile} />
						<ProfileBasicInfo userProfile={userProfile} />
						<Followers userProfile={userProfile} />
						<Following userProfile={userProfile} />
						<Photos userProfile={userProfile} />
						<Videos userProfile={userProfile} />
					</>
				) : null}
			</View>
		</ScrollView>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// padding: '5%',
		backgroundColor: '#E9EBEC',
		// paddingTop: 40
	},
});

export default ProfileHome;
