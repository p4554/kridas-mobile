import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { Divider, TextInput } from 'react-native-paper';
import CountryPicker from '../../components/CountryPicker';
import { FormInputDate } from '../../components/forms/FormInputDate';
import FormInputText from '../../components/forms/FormInputText';
import GenderPicker from '../../components/GenderPicker';
import StatePicker from '../../components/StatePicker';
import envs from '../../config/env';
import imagesPath from '../../constants/imagesPath';
import Back from '../../images/back.svg';

function PMBasicInfoEdit({ modalOpen, setModalOpen, currentUser }: any) {
	const { BACKEND_URL } = envs;

	const [dob, setDob] = useState('');
	const [date, setDate] = useState(new Date());
	const [show, setShow] = useState(false);
	const [mode, setMode] = useState('date');

	const onHandleShowDatePicker = (currentMode: any) => {
		setShow(true);
		setMode(currentMode);
	};

	const datePicker = (event: any, selectedDate: any) => {
		// console.log(event, selectedDate);
		const currentDate = selectedDate || date;
		setShow(Platform.OS === 'ios');
		setDate(currentDate);

		let tempDate = new Date(currentDate);
		let fDate = tempDate.getFullYear() + '-' + (tempDate.getMonth() + 1) + '-' + tempDate.getDate();
		setDob(fDate);
		setValue('dob', tempDate);
	};

	const {
		setValue,
		control,
		handleSubmit,
		formState: { errors },
	} = useForm();

	const [countryArray, setCountryArray] = useState<any>([]);

	const [openDropDown, setOpenDropDown] = useState(false);

	const [countryStates, setCountryStates] = useState([]);

	// modal window open / close
	const [openModal, setOpenModal] = useState(false);
	const [stateOpenModal, setStateOpenModal] = useState(false);
	const [openGenderModal, setOpenGenderModal] = useState(false);

	// selected country and state & gender
	const [selectedCountry, setSelectedCountry] = useState<any>(null);
	const [selectedState, setSelectedState] = useState<any>(null);
	const [selectedGender, setSelectedGender] = useState<any>(null);

	// errors
	const [errCountryState, setErrCountryState] = useState('');
	const [errCountry, setErrCountry] = useState('');
	const [errGender, setErrGender] = useState('');

	useEffect(() => {
		getCountryArray();

		// re-init errors states
		setErrCountryState('');
		setErrCountry('');
		setErrGender('');

		setValue('firstName', currentUser?.first_name);
		setValue('lastName', currentUser?.last_name);
		setValue('email', currentUser?.user_email);
		setValue('phone', currentUser?.user_phone);
		setValue('email', currentUser?.user_email);
		setValue('AddressLineOne', currentUser?.address?.line1);
		setValue('AddressLineTwo', currentUser?.address?.line2);
		setValue('pinCode', currentUser?.address?.pincode);
		setValue('city', currentUser?.address?.city);

		//set date of birth
		let tempDate = new Date(currentUser?.user_dob);
		let fDate = tempDate.getFullYear() + '-' + (tempDate.getMonth() + 1) + '-' + tempDate.getDate();
		setDob(fDate);

		setValue('dob', tempDate);

		setValue('user_gender', currentUser?.user_gender);

		setSelectedGender(currentUser?.user_gender);

		// initialize country & state drop down fetching values from currentUser
		setSelectedCountry(JSON.parse(currentUser?.countryData));

		let country_states = JSON.parse(currentUser?.countryData).country_states;
		setSelectedState(country_states.find((e: any) => e.state_code === currentUser?.address?.state));

		// populates all states of selected country
		setCountryStates(country_states);
	}, []);

	const getCountryArray = async () => {
		const response = await fetch(`${BACKEND_URL}/country/getAll`);
		const result = await response.json();

		let country_array = result.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each sport

		setCountryArray(country_array);
	};

	const onPress = () => {
		setOpenDropDown(false);
	};

	const handleSelectedCountry = (value: any) => {
		//re - initialize the variables
		setErrCountryState('');
		setErrCountry('');
		setCountryStates([]);
		setSelectedCountry(value);

		setOpenModal(false);

		// reset all checked flag to false
		let country_array = countryArray.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each element

		// compare selected country with each element and set checked to true
		setCountryArray(
			country_array.map((item: { country_id: any; checked: boolean }) =>
				item.country_id === value.country_id ? { ...item, checked: !item.checked } : item
			)
		);

		let states = value.country_states;
		setCountryStates(states.length > 0 ? states : []);
		setSelectedState(null);
	};

	const handleSelectedState = (value: any) => {
		setSelectedState(value);
		setStateOpenModal(false);
	};

	const handleSelectedGender = (value: any) => {
		setSelectedGender(value);
		setOpenGenderModal(false);
	};

	const handleStateClicked = () => {
		if (countryStates.length > 0) {
			setErrCountryState('');
			setStateOpenModal(!stateOpenModal);
		} else {
			setErrCountryState('Please select country first');
		}
	};

	const updateValue = async (data: any) => {
		if (selectedCountry === null && selectedState === null) {
			setErrCountry('Please select country');
			setErrCountryState('Please select state');
			return false;
		} else if (selectedCountry === null) {
			setErrCountry('Please select country');
			return false;
		} else if (selectedState === null) {
			setErrCountryState('Please select state');
			return false;
		}

		let addressObj = {
			country: selectedCountry?.id,
			state: selectedState?.id,
			line1: data.AddressLineOne,
			line2: data.AddressLineTwo,
			pincode: data.pinCode,
			city: data.city,
		};

		let formData = new FormData();

		const userType = currentUser.user_type;
		const userTypeValue = JSON.stringify(userType);
		const sportsInterest = currentUser.sports_interested;
		const sportsInterestValue = JSON.stringify(sportsInterest);
		let countryValue = selectedCountry?.country_code;
		// console.log(sportsValue)

		let stateValue = selectedState?.state_code;
		let genderValue = selectedGender;

		let obj = {
			country: countryValue,
			state: stateValue,
			line1: data.AddressLineOne,
			line2: data.AddressLineTwo,
			pincode: data.pinCode,
			city: data.city,
		};

		let details = JSON.stringify(obj);

		formData.append('first_name', data.firstName);
		formData.append('last_name', data.lastName);
		formData.append('user_email', data.email);
		formData.append('user_type', userTypeValue);
		formData.append('user_id', currentUser.user_id);
		formData.append('user_dob', data.dob);
		formData.append('sports_interested', sportsInterestValue);
		formData.append('user_phone', data.phone);
		// formData.append('bio_details', userProfile.bio_details);
		formData.append('user_gender', genderValue);
		formData.append('address', details);

		let requestOptions = {
			method: 'PUT',
			headers: {
				Accept: 'application/json',
			},
			body: formData,
		};
		//	console.log('dinesh### ' + JSON.stringify(formData));

		fetch(`${BACKEND_URL}users`, requestOptions)
			.then((response) => response.json())
			.then(async (result) => {
				//	console.log(result);
				//const userValue = JSON.stringify(result);

				setModalOpen(false);
			})
			.catch((error) => {
				console.log('error# ', error);
			});
	};

	return (
		<>
			<Modal
				isVisible={modalOpen}
				style={{ margin: 0, backgroundColor: '#fff' }}
				onBackdropPress={() => setModalOpen(false)}
				onBackButtonPress={() => setModalOpen(false)}
				backdropOpacity={1}
				backdropColor="#fff"
				hideModalContentWhileAnimating={true}
			>
				<View style={styles.header}>
					<View>
						<TouchableOpacity style={styles.headerContent} onPress={() => setModalOpen(!modalOpen)}>
							<Back style={styles.backIcon} />
							<Text style={styles.headerText}>Edit basic info</Text>
						</TouchableOpacity>
					</View>
					<View>
						<TouchableOpacity onPress={handleSubmit(updateValue)}>
							<Text style={styles.buttonText}>Save</Text>
						</TouchableOpacity>
					</View>
				</View>
				<Divider />
				<ScrollView>
					<View style={styles.container}>
						<View style={{ flex: 1 }}>
							{/* <Img style={styles.image}/> */}
							<Text style={styles.title}>COMPLETE YOUR PROFILE</Text>
						</View>
						<View style={styles.input}>
							<View>
								<View>
									<View>
										<FormInputText
											name="firstName"
											secureTextEntry={false}
											control={control}
											// label="First Name"
											placeHolder="Enter your first name"
											autoComplete="off"
											dense={true}
											autoFocus={false}
											mode="flat"
											onCustomChange={null}
											rules={{
												required: 'First name is required',
											}}
											right={<TextInput.Icon name={'account'} />}
										/>
									</View>
								</View>

								<View style={styles.paddingTopElement}>
									<FormInputText
										name="lastName"
										secureTextEntry={false}
										control={control}
										label="Last Name"
										placeHolder="Enter your last name"
										autoComplete="off"
										dense={true}
										autoFocus={false}
										mode="flat"
										onCustomChange={null}
										rules={{
											required: 'Last Name is required',
										}}
										right={<TextInput.Icon name={'account'} />}
									/>
								</View>

								<View style={styles.paddingTopElement}>
									<FormInputText
										name="email"
										secureTextEntry={false}
										control={control}
										label="Email"
										placeHolder="Enter your email"
										autoComplete="off"
										dense={true}
										autoFocus={false}
										mode="flat"
										onCustomChange={null}
										rules={{
											required: 'Email is required',
											pattern: {
												value: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
												message: 'Please enter a valid email address',
											},
										}}
										right={<TextInput.Icon name={'email-outline'} />}
										keyboardType={'email-address'}
									/>
								</View>

								<View style={styles.paddingTopElement}>
									<FormInputText
										name="phone"
										secureTextEntry={false}
										control={control}
										label="Contact number"
										placeHolder="Contact number"
										autoComplete="off"
										dense={true}
										autoFocus={false}
										mode="flat"
										onCustomChange={null}
										rules={{
											required: 'Contact number is required',
										}}
										// right={<TextInput.Icon name={'email-outline'} />}
										keyboardType={'number-pad'}
									/>
								</View>
							</View>

							<View>
								<View style={styles.paddingTopElement}>
									<View>
										<TouchableOpacity
											style={styles.dropDownStyle}
											activeOpacity={0.8}
											onPress={() => setOpenGenderModal(!openGenderModal)}
										>
											<Text>
												{!!selectedGender && selectedGender === 'M'
													? 'Male'
													: selectedGender === 'F'
													? 'Female'
													: selectedGender === 'O'
													? 'Other'
													: 'Select your gender'}
											</Text>
											<Image source={imagesPath.icDropDown} />
										</TouchableOpacity>
										{errGender && errGender !== '' ? (
											<View>
												<Text style={styles.errorText}>{errGender}</Text>
											</View>
										) : null}
									</View>

									<GenderPicker
										setOpenGenderModal={setOpenGenderModal}
										openGenderModal={openGenderModal}
										value={selectedGender}
										setValue={handleSelectedGender}
									/>
								</View>

								<View style={styles.paddingTopElement}>
									<FormInputDate
										name="dob"
										control={control}
										label={'Date of Birth'}
										placeHolder="DOB (yyyy-mm-dd)"
										defaultValue={dob}
										dense={true}
										onCustomChange={datePicker}
										rules={{
											required: 'Date of Birth is required',
										}}
										onHandleShowDatePicker={onHandleShowDatePicker}
										showDatePicker={show}
										dtValue={dob}
										dateMode={mode}
									/>
								</View>
							</View>

							<View>
								<View style={styles.paddingTopElement}>
									<FormInputText
										name="AddressLineOne"
										secureTextEntry={false}
										control={control}
										label="Address line one"
										placeHolder="Enter address"
										autoComplete="off"
										dense={true}
										autoFocus={false}
										mode="flat"
										onCustomChange={null}
										rules={{
											required: 'Line one is required',
										}}
										// right={<TextInput.Icon name={'account'} />}
									/>
								</View>

								<View style={styles.paddingTopElement}>
									<FormInputText
										name="AddressLineTwo"
										secureTextEntry={false}
										control={control}
										label="Address line two"
										placeHolder="Enter address"
										autoComplete="off"
										dense={true}
										autoFocus={false}
										mode="flat"
										onCustomChange={null}
										// rules={{
										//     required: 'Line one is required',
										// }}
										// right={<TextInput.Icon name={'account'} />}
									/>
								</View>

								<View style={styles.paddingTopElement}>
									<FormInputText
										name="pinCode"
										secureTextEntry={false}
										control={control}
										label="Pin code"
										placeHolder="Enter Pin code"
										autoComplete="off"
										dense={true}
										autoFocus={false}
										mode="flat"
										onCustomChange={null}
										rules={{
											required: 'Pin code is required',
										}}
										// right={<TextInput.Icon name={'email-outline'} />}
										keyboardType={'number-pad'}
									/>
								</View>

								<View style={styles.paddingTopElement}>
									<View>
										<TouchableOpacity
											style={styles.dropDownStyle}
											activeOpacity={0.8}
											onPress={() => setOpenModal(!openModal)}
										>
											<Text>
												{' '}
												{!!selectedCountry ? selectedCountry?.country_name : 'Choose a Country'}
											</Text>
											<Image source={imagesPath.icDropDown} />
										</TouchableOpacity>
										{errCountry && errCountry !== '' ? (
											<View>
												<Text style={styles.errorText}>{errCountry}</Text>
											</View>
										) : null}

										<View style={{ paddingTop: 24 }}>
											<TouchableOpacity
												style={{ ...styles.dropDownStyle }}
												activeOpacity={0.8}
												onPress={handleStateClicked}
											>
												<Text>
													{' '}
													{!!selectedState ? selectedState?.state_name : 'Choose a State'}
												</Text>
												<Image source={imagesPath.icDropDown} />
											</TouchableOpacity>
										</View>
										{errCountryState && errCountryState !== '' ? (
											<View>
												<Text style={styles.errorText}>{errCountryState}</Text>
											</View>
										) : null}

										<CountryPicker
											setOpenModal={setOpenModal}
											openModal={openModal}
											value={selectedCountry}
											setValue={handleSelectedCountry}
											items={countryArray}
											displayElement={'country_name'}
										/>
										{countryStates?.length > 0 && (
											<StatePicker
												setStateOpenModal={setStateOpenModal}
												stateOpenModal={stateOpenModal}
												stateValue={selectedState}
												setStateValue={handleSelectedState}
												items={countryStates}
												displayElement={'state_name'}
											/>
										)}
									</View>
								</View>

								<View style={styles.lastElement}>
									<FormInputText
										name="city"
										secureTextEntry={false}
										control={control}
										label="City/Town"
										placeHolder="Enter the city"
										autoComplete="off"
										dense={true}
										autoFocus={false}
										mode="flat"
										onCustomChange={null}
										rules={{
											required: 'City is required',
										}}
										// right={<TextInput.Icon name={'account'} />}
									/>
								</View>
							</View>
						</View>
					</View>
				</ScrollView>
			</Modal>
		</>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: '5%',
	},

	title: {
		fontSize: 16,
		fontWeight: 'bold',
		letterSpacing: 3.5,
		color: '#949494',
	},

	input: {
		paddingTop: 20,
	},
	paddingTopElement: {
		paddingTop: 20,
	},
	lastElement: {
		paddingTop: 20,
		paddingBottom: 20,
	},
	header: {
		padding: '5%',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	headerContent: {
		display: 'flex',
		flexDirection: 'row',
	},
	headerText: {
		fontSize: 16,
		paddingLeft: 10,
	},
	backIcon: {
		paddingTop: 23,
	},
	buttonText: {
		fontSize: 16,
		color: '#2F80ED',
	},
	dropDownStyle: {
		backgroundColor: '#fff',
		padding: 8,

		minHeight: 42,

		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',

		borderBottomColor: '#7a7a7a',
		borderBottomWidth: 1,
	},
	errorText: {
		color: 'red',
		fontSize: 12,
		paddingLeft: 5,
	},
});

export default PMBasicInfoEdit;
