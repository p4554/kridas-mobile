import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import envs from '../../config/env';
import Edit from '../../images/edit.svg';
import PMSportsStatAdd from './PMSportsStatAdd';
import SportsStatEditScreen from './SportsStatEditScreen';

const PMSportsStat = ({ userProfile }: any) => {
    const { BACKEND_URL } = envs;
    const [openModal, setOpenModal] = useState(false);
    const [addOpenModal, setAddOpenModal] = useState(false);
    const user_id = userProfile.user_id;
    const [statistics, setStatistics] = useState<any>([]);
    const [editProps, setEditProps] = useState<any>();

	useEffect(() => {
		(async function () {
			const response = await fetch(`${BACKEND_URL}/users/statistics/getUserById/${user_id}`);
			const result = await response.json();
			const resultData = result.data;
            // console.log("fff", resultData)
            // let idObj = {}
            // // console.log("f", resultData);
            // const stat_id = resultData.map((id: any) => {
            //     return id?.user_statistics_id
            // })
            // const id = resultData.map((i: any) => ({
            //     stat_id: i?.user_statistics_id,
            //     sports_id: i?.sports_id,
            //     skill_level: i?.skill_level,
            //     playing_status: i?.playing_status
            // }))
            // for(let i = 0; i < id.length; i++ ) {
            //     Object.assign(idObj, id[i]);
            // }
            // // console.log("obj", idObj)
            // let statId = stat_id[0]
            // setStatisticsId(idObj)
            // console.log("id", statId); 
            setStatistics(resultData);
            // let link = resultData?.statistics_links
            // console.log(link)
            console.log("stat", statistics);
		})();
	}, [openModal]);

    const onClickEditButton = (index: any) => {
        // const id = statistics.map((value: any) => ({
        //     id: value?.sports-id
        // }))
        // console.log("id", id)
        let getIndexValue = statistics[index]

        // console.log("value", getIndexValue )
        setEditProps(getIndexValue)
        setOpenModal(!openModal)
    }
    
  return (
    <>
        <View style={styles.content}>
            <View style={styles.title}>
                <Text
                    style={{
                        fontSize: 14,
                        fontWeight: 'bold',
                        color: '#949494',
                        letterSpacing: 2,
                    }}
                >
                    SPORTS YOU PLAY & SCORES
                </Text>
            </View>
            {statistics.map((item: any, index: any) => {
                return (
                    <>
                        <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                        <View>
                            <View>
                                <Text style={styles.subTitle}>Sports</Text>
                                <Text style={styles.details}>{item?.sports_name}</Text>
                            </View>
                            <View>
                                <Text style={styles.subTitle}>Skill Level</Text>
                                <Text style={styles.details}>{item?.category_name}</Text>
                            </View>
                            <View>
                                <Text style={styles.subTitle}>Active</Text>
                                {item?.playing_status === "AC" ? <Text style={styles.details}>I am actively playing</Text> : <Text style={styles.details}>I am not actively playing</Text>}
                                {/* <Text style={styles.details}>{item?.playing_status}</Text> */}
                            </View>
                            <View style={{ paddingTop: 30 }}>
                                <Text style={styles.heading}>External Stats URL (optional)</Text>
                                {JSON.parse(item?.statistics_links).map((link: any) => {
                                    return <Text style={styles.details}>{link}</Text>
                                })}
                                {/* {item?.url.map((link: any) => {
                                    return (
                                        <Text style={styles.details}>{link?.stat_link}</Text>
                                    )
                                })} */}
										{/* <Text style={styles.details}>1.http://www.football.co/ii9322</Text>
                                <Text style={styles.details}>2. http://www.football.co/ii9322</Text> */}
									</View>
									<View style={{ paddingTop: 30 }}>
										<Text style={styles.heading}>Certificates /Documents/Proofs</Text>
										<Text style={styles.details}>None</Text>
									</View>
								</View>
								<View style={{ paddingTop: 25 }}>
									<TouchableOpacity
										style={{ paddingRight: 20 }}
										// onPress={() => setOpenModal(!openModal)}1
                                        onPress={() => onClickEditButton(index)}
									>
										<Edit />
									</TouchableOpacity>
									<SportsStatEditScreen
										setModalOpen={setOpenModal}
										modalOpen={openModal}
										userProfile={userProfile}
										// statisticsId={statisticsId}
                                        editProps={editProps}
										statistics={statistics}
									/>
								</View>
							</View>
						</>
					);
				})}
				<View style={{ paddingTop: 20 }}>
					<TouchableOpacity style={{ paddingRight: 20 }} onPress={() => setAddOpenModal(!addOpenModal)}>
						<Text style={{ fontSize: 18, color: '#2F80ED' }}>Add Sports</Text>
					</TouchableOpacity>
					{addOpenModal ? (
						<PMSportsStatAdd
							setAddModalOpen={setAddOpenModal}
							addModalOpen={addOpenModal}
							userProfile={userProfile}
						/>
					) : null}
				</View>
			</View>
		</>
	);
};

export default PMSportsStat;

const styles = StyleSheet.create({
	content: {
		padding: '5%',
		backgroundColor: '#FFFFFF',
	},
	title: {
		paddingTop: 10,
		paddingBottom: 10,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	subTitle: {
		fontSize: 13,
		fontWeight: 'bold',
		color: '#999999',
		paddingTop: 12,
	},

	details: {
		fontSize: 16,
		fontWeight: '400',
		paddingTop: 5,
	},
	heading: {
		fontSize: 16,
		fontWeight: 'bold',
		color: '#555555',
	},
});
