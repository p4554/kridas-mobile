import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Divider, Snackbar, TextInput } from 'react-native-paper';
import FormInputText from '../components/forms/FormInputText';
import envs from '../config/env';
import useAsyncStorage from '../hooks/use-local-storage';
import Logo from '../images/logo.svg';

function LoginScreen({ navigation }: { navigation: any }) {
	const [currentUser, setCurrentUser] = useAsyncStorage('currentUser', '');
	const [token, setToken] = useAsyncStorage('token', '');

	const { BACKEND_URL } = envs;

	const [isSignedIn, setIsSignedIn] = useState(false);

	const [passwordVisible, setPasswordVisible] = useState(false);
	const [visible, setVisible] = React.useState(false);
	const onToggleSnackBar = () => setVisible(!visible);

	const onDismissSnackBar = () => setVisible(false);

	const {
		control,
		handleSubmit,
		formState: { errors },
	} = useForm();

	const onLoginInPressed = async (data: any) => {
		//	console.log(data);

		let payload = { email: data.email, password: data.password };

		let result = await authCheck(payload);
	};

	const authCheck = async (payload: any) => {
		//	console.log('BACKEND_URL ::>> ', envs);
		//	console.log('__DEV__ ', __DEV__);

		await axios
			.post(`${BACKEND_URL}users/login`, payload)
			.then(async (response) => {
				let result = response?.data;
				// console.log(result);

				const sports = JSON.stringify(result?.user?.sports_interested);
				//	console.log('sports--->', sports);
				const userValue = JSON.stringify(result?.user);
				const userToken = JSON.stringify(result?.token);
				//	console.log('t', userToken);
				// console.log("userValue======>",userValue)
				await AsyncStorage.setItem('currentUser', userValue);
				await AsyncStorage.setItem('token', userToken);

				setCurrentUser?.(userValue);
				setToken?.(userToken);

				if (result.isNewUser) {
					navigation.navigate('SportsSelectionScreen');
				} else {
					navigation.navigate('Tab');
				}
			})
			.catch((err) => {
				// Handle error
				onToggleSnackBar();
			});
	};

	useEffect(() => {
		(async function () {
			const value = await AsyncStorage.getItem('token');
			//	console.log('tokenOne---->', value);
			if (value !== null) {
				setIsSignedIn(true);
				navigation.navigate('Tab');
			} else {
				console.log('login failed');
			}
		})();
	}, []);

	return (
		<View style={styles.container}>
			<View style={styles.positionLogo}>
				<Logo />
			</View>
			<Text style={styles.signInLabel}>Sign In</Text>
			<View style={styles.input}>
				<View>
					<FormInputText
						name="email"
						secureTextEntry={false}
						control={control}
						label="Email"
						placeHolder="Enter your email"
						autoComplete="off"
						dense={true}
						autoFocus={true}
						mode="flat"
						onCustomChange={null}
						rules={{
							required: 'Email is required',
							pattern: {
								value: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
								message: 'Please enter a valid email address',
							},
						}}
						right={<TextInput.Icon name={'email-outline'} />}
						keyboardType={'email-address'}
					/>
				</View>
				<View style={styles.elementTopPadding}>
					<FormInputText
						name="password"
						secureTextEntry={passwordVisible}
						control={control}
						label="Password"
						placeHolder="Enter your password"
						autoComplete="off"
						dense={true}
						autoFocus={false}
						mode="flat"
						onCustomChange={null}
						rules={{
							required: 'Password is required',
							minLength: { value: 6, message: 'Please enter a valid password' },
						}}
						right={
							<TextInput.Icon
								name={passwordVisible ? 'eye-off' : 'eye'}
								onPress={() => setPasswordVisible(!passwordVisible)}
							/>
						}
					/>
				</View>
			</View>
			<View style={{ width: '100%' }}>
				<Text style={styles.textOne}>Forgot Password?</Text>
			</View>

			<View style={styles.signInButton}>
				<TouchableOpacity onPress={handleSubmit(onLoginInPressed)}>
					<View style={styles.buttonOne}>
						<Text style={styles.buttonText}>Sign In</Text>
					</View>
				</TouchableOpacity>
			</View>
			<View>
				<Text style={styles.registeredInfo}>By signing in you are agreeing to our terms and conditions</Text>
			</View>
			<Divider style={{ width: '100%' }} />
			<View>
				<Text style={styles.signUpText}>New to SportsPlatform?</Text>
			</View>
			<View style={styles.registerButton}>
				<TouchableOpacity onPress={() => navigation.navigate('SignUpScreen')}>
					<View style={styles.buttonTwo}>
						<Text style={styles.registerButtonText}>+ Register now</Text>
					</View>
				</TouchableOpacity>
			</View>

			<Snackbar
				visible={visible}
				onDismiss={onDismissSnackBar}
				theme={{
					colors: {
						onSurface: 'rgb(255,0,0)', // background color
						surface: '#fff', // font color
					},
				}}
			>
				Invalid Credentials!
			</Snackbar>
		</View>
	);
}

// onPress={() => navigation.navigate("HomeScreen")}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: '5%',
	},

	positionLogo: {
		alignItems: 'center',
		padding: 10,
		paddingTop: 20,
	},

	signInLabel: {
		fontWeight: '700',
		fontSize: 20,
		letterSpacing: 1,
		paddingTop: 90,
		paddingBottom: 30,
	},

	input: {
		width: '100%',
		alignSelf: 'center',
	},
	elementTopPadding: {
		paddingTop: 20,
	},
	textInput: {
		borderBottomColor: '#0062BD',
		borderBottomWidth: 2,
	},

	textOne: {
		fontWeight: '400',
		fontSize: 16,
		color: '#0062BD',
		// marginLeft: Dimensions.get('window').width / 2,
		paddingTop: 5,
		alignSelf: 'flex-end',
	},

	signInButton: {
		paddingTop: 30,
		// marginLeft: Dimensions.get('window').width / 6
		alignSelf: 'center',
		width: '100%',
	},

	buttonOne: {
		borderRadius: 2,
		paddingVertical: 14,
		// paddingHorizontal: 10,
		backgroundColor: '#0062BD',
		// width: "80%"
	},

	buttonText: {
		color: 'white',
		fontSize: 18,
		lineHeight: 24,
		textAlign: 'center',
	},

	registeredInfo: {
		fontSize: 10,
		color: '#AAAAAA',
		// marginLeft: 8*vh,
		paddingBottom: 20,
		// marginLeft: Dimensions.get('window').width / 6,
		paddingTop: 15,
		alignSelf: 'center',
	},

	signUpText: {
		fontWeight: '400',
		fontSize: 18,
		paddingTop: 30,
		alignSelf: 'center',
	},

	registerButton: {
		// marginLeft: 8*vh,
		// marginLeft: Dimensions.get('window').width / 6,
		paddingTop: 24,
		alignSelf: 'center',
		width: '100%',
	},
	buttonTwo: {
		borderRadius: 2,
		borderWidth: 2,
		borderColor: '#0062BD',
		paddingVertical: 14,
		// paddingHorizontal: 10,
		backgroundColor: 'white',
		// width: "80%"
	},
	registerButtonText: {
		fontWeight: '400',
		fontSize: 18,
		alignSelf: 'center',
		color: '#2F80ED',
	},
});

export default LoginScreen;
