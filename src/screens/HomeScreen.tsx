import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import FormInputText from '../components/forms/FormInputText';
import { useUser } from '../hooks/user-hooks';
import DocLink from '../images/doc-link.svg';
import ImageLink from '../images/image-link.svg';
import Img from '../images/unsplash_2EJCSULRwC8.svg';
import VideoLink from '../images/video-link.svg';

function HomeScreen({ navigation }: { navigation: any }) {
	const { data: userData, error } = useUser('4d9d505d-836b-41e8-ab7a-5f7a6a1947b4');
	const [user, setUser] = useState('');
	const [token, setToken] = useState('');

	//	console.log('dinesh home screen... user.. ' + JSON.stringify(userData));

	const {
		control,
		handleSubmit,
		formState: { errors },
	} = useForm();

	const logout = async () => {
		await AsyncStorage.removeItem('token');
		await AsyncStorage.removeItem('currentUser');
		navigation.navigate('LoginScreen');
	};

	useEffect(() => {
		(async function () {
			const value = await AsyncStorage.getItem('token');
			//	console.log('tokenOne---->', value);
		})();
	}, []);

	return (
		<ScrollView>
			<View style={styles.container}>
				<View style={styles.box}>
					<View>
						<Text style={styles.title}>Featured Events</Text>
						<View style={styles.eventContainer}>
							<View style={styles.eventBox}>
								<Img width="100%" />
								<View style={styles.boxPadding}>
									<Text style={styles.eventName}>Dakar Volleyball 2022</Text>
									<Text style={styles.eventLocation}>Dakar, Sharjah</Text>
								</View>
							</View>
							<View style={styles.eventBox}>
								<Img width="100%" />
								<View style={styles.boxPadding}>
									<Text style={styles.eventName}>Dakar Volleyball 2022</Text>
									<Text style={styles.eventLocation}>Dakar, Sharjah</Text>
								</View>
							</View>
						</View>
					</View>
					<View style={styles.elementTopPadding}>
						<Text style={styles.title}>Featured Articles</Text>
						<View style={styles.eventContainer}>
							<View style={styles.eventBox}>
								<Img width="100%" />
								<View style={styles.boxPadding}>
									<Text style={styles.eventName}>Dakar Volleyball 2022</Text>
									<Text style={styles.eventLocation}>Dakar, Sharjah</Text>
								</View>
							</View>
							<View style={styles.eventBox}>
								<Img width="100%" />
								<View style={styles.boxPadding}>
									<Text style={styles.eventName}>Dakar Volleyball 2022</Text>
									<Text style={styles.eventLocation}>Dakar, Sharjah</Text>
								</View>
							</View>
						</View>
					</View>
				</View>
				<View style={styles.profileInfo}>
					<View style={styles.profileBox}>
						<View>
							<View></View>
							<View style={styles.selectButton}>
								<TouchableOpacity>
									<View style={styles.button}>
										<Text style={styles.buttonText}>+ Register now</Text>
									</View>
								</TouchableOpacity>
							</View>
						</View>
					</View>
				</View>
				<View style={styles.elementTopPadding}>
					<View style={styles.feed}>
						<Text style={styles.title}>Your Feed</Text>
						<View style={styles.profileBox}>
							<View>
								<View>
									<Image source={require('../images/profileImage.png')} />
								</View>
								<View>
									<View>
										<FormInputText
											name="feed"
											secureTextEntry={false}
											control={control}
											// label="What’s on your mind user?"
											placeHolder="What’s on your mind user?"
											autoComplete="off"
											dense={true}
											autoFocus={true}
											mode="outlined"
											onCustomChange={null}
											multiline={true}
											rules={{
												required: 'Please enter the text',
											}}
										/>
									</View>
									<View style={styles.uploads}>
										<ImageLink style={{ marginLeft: 20 }} />
										<VideoLink style={{ marginLeft: 20 }} />
										<DocLink style={{ marginLeft: 20 }} />
									</View>
								</View>
							</View>
						</View>
					</View>
				</View>
				<View style={styles.logOutButton}>
					<TouchableOpacity onPress={logout}>
						<View style={styles.buttonTwo}>
							<Text style={styles.logOutButtonText}>Log Out</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		</ScrollView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#E9EBEC',
	},
	box: {
		padding: '5%',
		backgroundColor: 'white',
	},
	elementTopPadding: {
		paddingTop: 20,
	},
	title: {
		fontSize: 16,
		lineHeight: 24,
		color: '#555555',
	},
	eventContainer: {
		paddingTop: 8,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-around',
	},
	eventBox: {
		backgroundColor: '#F5F5F5',
		borderRadius: 10,
		borderWidth: 1,
		width: '46.5%',
		borderColor: 'white',
	},
	boxPadding: {
		paddingTop: 8,
		paddingBottom: 20,
	},
	eventName: {
		fontSize: 14,
		marginLeft: 8,
		color: '#333333',
	},

	eventLocation: {
		fontSize: 16,
		marginLeft: 8,
		color: '#333333',
	},
	profileInfo: {
		paddingTop: 20,
		padding: '5%',
	},
	profileBox: {
		backgroundColor: 'white',
		borderRadius: 10,
		padding: '5%',
	},
	profileTitle: {
		fontSize: 16,
	},
	selectButton: {
		paddingTop: 24,
		alignSelf: 'center',
		width: '60%',
	},
	button: {
		borderRadius: 2,
		borderWidth: 2,
		borderColor: '#0062BD',
		paddingVertical: 14,
		backgroundColor: 'white',
	},
	buttonText: {
		fontWeight: '400',
		fontSize: 18,
		alignSelf: 'center',
		color: '#2F80ED',
	},
	feed: {
		padding: '5%',
	},
	uploads: {
		display: 'flex',
		flexDirection: 'row',
		paddingTop: 18,
	},
	logOutButton: {
		// marginLeft: 8*vh,
		// marginLeft: Dimensions.get('window').width / 6,
		paddingTop: 24,
		alignSelf: 'center',
		width: '80%',
	},
	buttonTwo: {
		borderRadius: 2,
		borderWidth: 2,
		borderColor: '#0062BD',
		paddingVertical: 14,
		// paddingHorizontal: 10,
		backgroundColor: 'white',
		// width: "80%"
	},
	logOutButtonText: {
		fontWeight: '400',
		fontSize: 18,
		alignSelf: 'center',
		color: '#2F80ED',
	},
});

export default HomeScreen;
