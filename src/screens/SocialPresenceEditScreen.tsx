import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Modal, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Divider } from 'react-native-paper';
import FormInputText from '../components/forms/FormInputText';

import { AutocompleteDropdown } from 'react-native-autocomplete-dropdown';
import Back from '../images/back.svg';

const socialMedia = [
	{
		id: 'FBK',
		name: 'Facebook',
	},
	{
		id: 'INS',
		name: 'Instagram',
	},
	{
		id: 'TWT',
		name: 'Twitter',
	},
	{
		id: 'LKN',
		name: 'LinedIn',
	},
];

const SocialPresenceEditScreen = ({ modalOpen, setModalOpen, userProfile }: any) => {
	const [user, setUser] = useState('');

	const [link, setLink] = useState('');
	const [description, setDescription] = useState('');
	const [social, setSocial] = useState('');
	const [socialLoading, setSocialLoading] = useState(false);
	const [socialSuggestionsList, setSocialSuggestionsList] = useState(null);
	const [selectedSocial, setSelectedSocial] = useState(null);

	const dropdownController = useRef(null);
	const searchRef = useRef(null);

	const {
		setValue,
		control,
		handleSubmit,
		formState: { errors },
	} = useForm();

	useEffect(() => {
		(async function () {
			const value = await AsyncStorage.getItem('currentUser');
			const userValue = JSON.parse(value || '{}');
			setUser(userValue);
			const detail = userValue.address;
		})();

		setValue('link', userProfile?.social?.link);
		setValue('description', userProfile?.social?.desc);
	}, []);

	const getSocialSuggestions = useCallback((q) => {
		console.log('getSuggestions', q);
		if (typeof q !== 'string') {
			setSocialSuggestionsList(null);
			return;
		}
		setSocialLoading(true);
		const items = socialMedia;
		const suggestions = items.map((item) => ({
			id: item?.id,
			title: item?.name,
		}));
		setSocialSuggestionsList(suggestions);
		setSocialLoading(false);
	}, []);

	const onSocialClearPress = useCallback(() => {
		setSocialSuggestionsList(null);
	}, []);

	const onSocialOpenSuggestionsList = useCallback((isOpened) => {}, []);

	const updateValue = async () => {
		const userType = user.user_type;
		const ut = JSON.stringify(userType);
		const sp = user.sports_interested;
		const si = JSON.stringify(sp);
		const address = user.address;
		const ad = JSON.stringify(address);
		const bioDetails = user.bio_details;
		const bio = JSON.stringify(bioDetails);

		let socialValue = selectedSocial.id;
		setSocial(socialValue);
		let obj = {
			desc: description,
			link: link,
			type: socialValue,
		};

		let details = JSON.stringify(obj);

		var formdata = new FormData();
		formdata.append('first_name', user.first_name);
		formdata.append('last_name', user.last_name);
		formdata.append('user_email', user.user_email);
		formdata.append('user_type', ut);
		formdata.append('user_dob', user.user_dob);
		formdata.append('user_id', user.user_id);
		formdata.append('referral_code', user.referral_code);
		formdata.append('user_name', user.user_name);
		formdata.append('user_profile_verified', user.user_profile_verified);
		formdata.append('user_status', user.user_status);
		formdata.append('sports_interested', si);
		formdata.append('user_phone', user.user_phone);
		formdata.append('user_gender', user.user_gender);
		formdata.append('address', ad);
		formdata.append('bio_details', bio);
		formdata.append('social', details);

		var requestOptions = {
			method: 'PUT',
			headers: {
				Accept: 'application/json',
				// "Content-Type": "application/json",
			},
			body: formdata,
			redirect: 'follow',
		};

		fetch('http://3.143.119.220:5001/api/users', requestOptions)
			.then((response) => response.json())
			.then(async (result) => {
				console.log(result);
				const userValue = JSON.stringify(result);
				await AsyncStorage.setItem('currentUser', userValue);
				if (result) {
					navigation.navigate('My Info');
				}
			})
			.catch((error) => console.log('error', error));
	};

	return (
		<>
			<Modal
				animationType="slide"
				transparent={false}
				visible={modalOpen}
				onRequestClose={() => {
					//	setModalOpen(!modalOpen);
				}}
			>
				<View style={styles.header}>
					<View>
						<TouchableOpacity style={styles.headerContent} onPress={() => setModalOpen(!modalOpen)}>
							<Back style={styles.backIcon} />
							<Text style={styles.headerText}>Edit Social Presence</Text>
						</TouchableOpacity>
					</View>
					<View>
						<TouchableOpacity onPress={updateValue}>
							<Text style={styles.buttonText}>Save</Text>
						</TouchableOpacity>
					</View>
				</View>
				<Divider />
				<ScrollView>
					<View style={styles.container}>
						<View style={{ flex: 1 }}>
							{/* <Img style={styles.image}/> */}
							<Text style={styles.title}>YOUR SOCIAL PRESENCE</Text>
							<Text style={{ fontSize: 12 }}>Required when applying for Sponsorship</Text>
						</View>
						<View style={styles.input}>
							<View>
								<View style={styles.textInput}>
									<AutocompleteDropdown
										ref={searchRef}
										controller={(controller) => {
											dropdownController.current = controller;
										}}
										textInputProps={{
											placeholder: 'Select Media',
											style: {
												backgroundColor: 'white',
												height: 35,
												width: '100%',
												marginBottom: 20,
												borderBottomColor: '#0062BD',
												borderBottomWidth: 2,
											},
										}}
										rightButtonsContainerStyle={{
											right: 68,
											color: 'red',
											backgroundColor: 'white',
											height: 30,
										}}
										suggestionsListContainerStyle={{
											width: '100%',
										}}
										onSelectItem={setSelectedSocial}
										dataSet={socialSuggestionsList}
										onChangeText={getSocialSuggestions}
										onClear={onSocialClearPress}
										onOpenSuggestionsList={onSocialOpenSuggestionsList}
										loading={socialLoading}
										useFilter={false}
										containerStyle={{ flexGrow: 1, flexShrink: 1 }}
										renderItem={(item) => (
											<Text style={{ color: '#383b42', padding: 15 }}>{item.title}</Text>
										)}
									/>
								</View>

								<View>
									<FormInputText
										name="link"
										secureTextEntry={false}
										control={control}
										// label="What’s on your mind user?"
										placeHolder="link"
										autoComplete="off"
										dense={true}
										autoFocus={true}
										mode="flat"
										onCustomChange={null}
										multiline={true}
										rules={{
											required: 'Please enter the link',
										}}
									/>
								</View>

								<View style={{ paddingTop: 20 }}>
									<FormInputText
										name="description"
										secureTextEntry={false}
										control={control}
										// label="What’s on your mind user?"
										placeHolder="description"
										autoComplete="off"
										dense={true}
										autoFocus={false}
										mode="outlined"
										onCustomChange={null}
										multiline={true}
										rules={{
											required: 'Please enter the description',
										}}
									/>
								</View>
							</View>
						</View>
						{/* <View style={styles.signInButton}>
                        <TouchableOpacity onPress={updateValue}>
                            <View style={styles.buttonOne}>
                                <Text style={styles.buttonText}>SUBMIT</Text>
                            </View>
                        </TouchableOpacity>
                    </View> */}
					</View>
				</ScrollView>
			</Modal>
		</>
	);
};

export default SocialPresenceEditScreen;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: '5%',
	},

	title: {
		fontSize: 16,
		fontWeight: 'bold',
		letterSpacing: 3.5,
		color: '#949494',
		paddingTop: 20,
	},

	input: {
		paddingTop: 20,
		// backgroundColor: "white"
	},
	headerContent: {
		display: 'flex',
		flexDirection: 'row',
	},
	headerText: {
		fontSize: 16,
		paddingLeft: 10,
	},
	backIcon: {
		paddingTop: 23,
	},
	header: {
		padding: '5%',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	buttonText: {
		fontSize: 16,
		color: '#2F80ED',
	},
});
