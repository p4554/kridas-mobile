import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Image, Modal, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Checkbox, Divider } from 'react-native-paper';
import { FormInputDate } from '../components/forms/FormInputDate';
import { FormInputDate2 } from '../components/forms/FormInputDate2';
import FormInputText from '../components/forms/FormInputText';
import SportsPicker from '../components/SportsPicker';
import SportsProfilePicker from '../components/sportsProfilePicker';
import SportsRolePicker from '../components/sportsRolePicker';
import envs from '../config/env';
import imagesPath from '../constants/imagesPath';
import Back from '../images/back.svg';

const SportsCareerEditScreen = ({ modalOpen, setModalOpen, userProfile }: any) => {
	const { BACKEND_URL } = envs;
	const [user, setUser] = useState('');
	// const [fromDate, setFromDate] = useState('');
	// const [toDate, setToDate] = useState('');
	const [fromDate, setFromDate] = useState('');
	const [toDate, setToDate] = useState('');
	const [date, setDate] = useState(new Date());
	const [showFromDate, setShowFromDate] = useState(false);
	const [showToDate, setShowToDate] = useState(false);
	const [mode, setMode] = useState('date');
	const [checked, setChecked] = useState(false);

	const [selectedSports, setSelectedSports] = useState<any>(null);
	const [selectedProfile, setSelectedProfile] = useState<any>(null);
	const [selectedRole, setSelectedRole] = useState<any>(null);

	const [profile, setProfile] = useState('');
	const [role, setRole] = useState('');

	const [selectedItem, setSelectedItem] = useState<any>(null);

	const [sportsArray, setSportsArray] = useState<any>([]);

	const [sports, setSports] = useState<any>(null);

	const [openModal, setOpenModal] = useState(false);

	const [openDropDown, setOpenDropDown] = useState(false);

	const [sportsProfile, setSportsProfile] = useState([]);
	const [profileOpenModal, setProfileOpenModal] = useState(false);

	const [sportsRole, setSportsRole] = useState([]);
	const [roleOpenModal, setRoleOpenModal] = useState(false);

	const [errSportsProfile, setErrSportsProfile] = useState('');
	const [errSportsRole, setErrSportsRole] = useState('');
	const [errSports, setErrSports] = useState('');

	const {
		setValue,
		control,
		handleSubmit,
		formState: { errors },
	} = useForm();

	useEffect(() => {
		(async function () {
			const value = await AsyncStorage.getItem('currentUser');
			const userValue = JSON.parse(value || '{}');
			setUser(userValue);
			const detail = userValue.address;

			getSportsArray();
		})();
		setErrSportsProfile('');
		setErrSportsRole('');
		setErrSports('');
	}, []);

	const getSportsArray = async () => {
		const response = await fetch(`${BACKEND_URL}/sports/getAll`);
		const result = await response.json();

		let sports_array = result.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each sport

		setSportsArray(sports_array);
		//console.log(JSON.stringify(countryArray));
	};

	const handleSelectedSports = (value: any) => {
		//re - initialize the variables
		setErrSportsProfile('');
		setErrSportsRole('');
		setErrSports('');
		setSportsProfile([]);
		setSportsRole([]);
		setSelectedSports(value);

		setOpenModal(false);

		// reset all checked flag to false
		let sports_array = sportsArray.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each element

		// compare selected country with each element and set checked to true
		setSportsArray(
			sports_array.map((item: { sports_id: any; checked: boolean }) =>
				item.sports_id === value.sports_id ? { ...item, checked: !item.checked } : item
			)
		);

		let profile = value.sports_profile;
		setSportsProfile(profile.length > 0 ? profile : []);
		console.log('s', sportsProfile);
		setSelectedProfile(null);

		let role = value.sports_role;
		setSportsRole(role.length > 0 ? role : []);
		console.log('s', sportsRole);
		setSelectedRole(null);
	};

	const handleSelectedProfile = (value: any) => {
		setSelectedProfile(value);
		setProfileOpenModal(false);
	};

	const handleProfileClicked = () => {
		if (sportsProfile.length > 0) {
			setErrSportsProfile('');
			setProfileOpenModal(!profileOpenModal);
		} else {
			setErrSportsProfile('Please select sports first');
		}
	};

	const handleSelectedRole = (value: any) => {
		setSelectedRole(value);
		setRoleOpenModal(false);
	};

	const handleRoleClicked = () => {
		if (sportsRole.length > 0) {
			setErrSportsRole('');
			setRoleOpenModal(!roleOpenModal);
		} else {
			setErrSportsRole('Please select sports first');
		}
	};

	const onHandleShowFromDatePicker = (currentMode: any) => {
		setShowFromDate(true);
		setMode(currentMode);
	};

	const onHandleShowToDatePicker = (currentMode: any) => {
		setShowToDate(true);
		setMode(currentMode);
	};

	const fromDatePicker = (event: any, selectedDate: any) => {
		// console.log(event, selectedDate);
		const currentDate = selectedDate || date;
		setShowFromDate(Platform.OS === 'ios');
		setDate(currentDate);

		let tempDate = new Date(currentDate);
		let fDate = tempDate.getFullYear() + '-' + (tempDate.getMonth() + 1) + '-' + tempDate.getDate();
		setFromDate(fDate);
		setValue('dob', tempDate);
	};

	const toDatePicker = (event: any, selectedDate: any) => {
		// console.log(event, selectedDate);
		const currentDate = selectedDate || date;
		setShowToDate(Platform.OS === 'ios');
		setDate(currentDate);

		let tempDate = new Date(currentDate);
		let fDate = tempDate.getFullYear() + '-' + (tempDate.getMonth() + 1) + '-' + tempDate.getDate();
		setToDate(fDate);
		setValue('to', tempDate);
	};

	const updateValue = async (data: any) => {
		const sportsSelected = selectedSports?.sports_id;
		const profileSelected = selectedProfile?.profile_code;
		const roleSelected = selectedRole?.role_code;

		let raw = JSON.stringify({
			user_id: userProfile.user_id,
			sports_id: sportsSelected,
			sports_career: [
				{
					name: data.clubName,
					url: data.link,
					profiles: profileSelected,
					roles: roleSelected,
					isCurrent: false,
					from: '',
					to: '',
					achievements: data.achievements,
				},
			],
		});

		let requestOptions = {
			method: 'POST',
			headers: {
				Accept: 'application/json',
			},
			body: raw,
		};

		fetch(`${BACKEND_URL}users/statistics/sportsCareer`, requestOptions)
			.then((response) => response.json())
			.then(async (result) => {
				console.log(result);
			})
			.catch((error) => {
				console.log('error# ', error);
			});
	};

	return (
		<>
			<Modal
				animationType="slide"
				transparent={false}
				visible={modalOpen}
				onRequestClose={() => {
					//	setModalOpen(!modalOpen);
				}}
			>
				<View style={styles.header}>
					<View>
						<TouchableOpacity style={styles.headerContent} onPress={() => setModalOpen(!modalOpen)}>
							<Back style={styles.backIcon} />
							<Text style={styles.headerText}>Edit Sports Career</Text>
						</TouchableOpacity>
					</View>
					<View>
						<TouchableOpacity onPress={updateValue}>
							<Text style={styles.buttonText}>Save</Text>
						</TouchableOpacity>
					</View>
				</View>
				<Divider />
				<ScrollView>
					<View style={styles.container}>
						<Text style={styles.title}>YOUR SPORTS CAREER</Text>
						<View style={styles.input}>
							<View>
								<View style={styles.centeredView}></View>
								<TouchableOpacity
									style={styles.dropDownStyle}
									activeOpacity={0.8}
									onPress={() => setOpenModal(!openModal)}
								>
									<Text> {!!selectedSports ? selectedSports?.sports_name : 'Choose a Sports'}</Text>
									<Image source={imagesPath.icDropDown} />
								</TouchableOpacity>
								{errSports && errSports !== '' ? (
									<View>
										<Text style={styles.errorText}>{errSports}</Text>
									</View>
								) : null}

								<SportsPicker
									setOpenModal={setOpenModal}
									openModal={openModal}
									value={selectedSports}
									setValue={handleSelectedSports}
									items={sportsArray}
									displayElement={'sports_name'}
								/>
							</View>
							<View style={styles.elementTopPadding}>
								<Text style={styles.sportsInfo}>Enter your sports career below</Text>
							</View>
							<View style={styles.elementTopPadding}>
								<FormInputText
									name="clubName"
									secureTextEntry={false}
									control={control}
									label="Org/Club Name"
									placeHolder="Enter Org/Club Name"
									autoComplete="off"
									dense={true}
									autoFocus={false}
									mode="flat"
									onCustomChange={null}
									rules={{
										required: 'Org/Club Name is required',
									}}
								/>
							</View>
							<View style={styles.elementTopPadding}>
								<FormInputText
									name="link"
									secureTextEntry={false}
									control={control}
									label="Org/Club URL/Website"
									placeHolder="Enter Org/Club URL/Website"
									autoComplete="off"
									dense={true}
									autoFocus={false}
									mode="flat"
									onCustomChange={null}
									rules={{
										required: 'Org/Club URL/Website is required',
									}}
								/>
							</View>
							<View style={styles.elementTopPadding}>
								<TouchableOpacity
									style={{ ...styles.dropDownStyle }}
									activeOpacity={0.8}
									onPress={handleProfileClicked}
								>
									<Text> {!!selectedProfile ? selectedProfile?.state_name : 'Choose a Profile'}</Text>
									<Image source={imagesPath.icDropDown} />
								</TouchableOpacity>
								{errSportsProfile && errSportsProfile !== '' ? (
									<View>
										<Text style={styles.errorText}>{errSportsProfile}</Text>
									</View>
								) : null}
								{sportsProfile?.length > 0 && (
									<SportsProfilePicker
										setProfileOpenModal={setProfileOpenModal}
										profileOpenModal={profileOpenModal}
										profileValue={selectedProfile}
										setProfileValue={handleSelectedProfile}
										items={sportsProfile}
										displayElement={'profile_name'}
									/>
								)}
							</View>
							<View style={styles.elementTopPadding}>
								<TouchableOpacity
									style={{ ...styles.dropDownStyle }}
									activeOpacity={0.8}
									onPress={handleRoleClicked}
								>
									<Text> {!!selectedRole ? selectedRole?.state_name : 'Choose a Role'}</Text>
									<Image source={imagesPath.icDropDown} />
								</TouchableOpacity>
								{errSportsRole && errSportsRole !== '' ? (
									<View>
										<Text style={styles.errorText}>{errSportsRole}</Text>
									</View>
								) : null}
								{sportsRole?.length > 0 && (
									<SportsRolePicker
										setRoleOpenModal={setRoleOpenModal}
										roleOpenModal={roleOpenModal}
										roleValue={selectedRole}
										setRoleValue={handleSelectedRole}
										items={sportsRole}
										displayElement={'role_name'}
									/>
								)}
							</View>
							<View style={styles.elementTopPadding}>
								<FormInputDate
									name="fromDate"
									control={control}
									label={'From date'}
									placeHolder="DOB (yyyy-mm-dd)"
									//defaultValue={fromDate}
									dense={true}
									onCustomChange={fromDatePicker}
									rules={{
										required: 'From date is required',
									}}
									onHandleShowDatePicker={onHandleShowFromDatePicker}
									showDatePicker={showFromDate}
									dtValue={fromDate}
									dateMode={mode}
								/>
							</View>
							<View style={styles.elementTopPadding}>
								<FormInputDate2
									name="toDate"
									control={control}
									label={'To Date'}
									placeHolder="DOB (yyyy-mm-dd)"
									defaultValue={toDate}
									dense={true}
									onCustomChange={toDatePicker}
									rules={{
										required: 'To Date is required',
									}}
									onHandleShowDatePicker={onHandleShowToDatePicker}
									showDatePicker2={showToDate}
									dtValue={toDate}
									dateMode={mode}
								/>
							</View>
							<View style={{ display: 'flex', flexDirection: 'row', paddingTop: 10 }}>
								<Checkbox
									status={checked ? 'checked' : 'unchecked'}
									onPress={() => {
										setChecked(!checked);
									}}
								/>
								<Text style={styles.checkboxText}>Current</Text>
							</View>
							<View style={styles.elementTopPadding}>
								<FormInputText
									name="achievements"
									secureTextEntry={false}
									control={control}
									// label="What’s on your mind user?"
									placeHolder="Achievements / Milestones"
									autoComplete="off"
									dense={true}
									autoFocus={false}
									mode="outlined"
									onCustomChange={null}
									multiline={true}
									rules={{
										required: 'Achievement is required',
									}}
								/>
							</View>
							<View style={styles.elementTopPadding}>
								<Text style={styles.add}>+ Add another Org/Club</Text>
							</View>
						</View>
					</View>
				</ScrollView>
			</Modal>
		</>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: '5%',
	},
	title: {
		fontSize: 16,
		fontWeight: 'bold',
		letterSpacing: 3.5,
		color: '#949494',
		paddingTop: 40,
	},
	sportsInfo: {
		fontSize: 12,
		color: '#555555',
	},
	input: {
		width: '100%',
		alignSelf: 'center',
	},
	elementTopPadding: {
		paddingTop: 20,
	},
	add: {
		fontSize: 16,
		color: '#2F80ED',
	},
	headerContent: {
		display: 'flex',
		flexDirection: 'row',
	},
	headerText: {
		fontSize: 16,
		paddingLeft: 10,
	},
	backIcon: {
		paddingTop: 23,
	},
	header: {
		padding: '5%',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	buttonText: {
		fontSize: 16,
		color: '#2F80ED',
	},
	Check: {
		display: 'flex',
		flexDirection: 'row',
	},
	checkboxText: {
		fontSize: 18,
		paddingLeft: 2,
		paddingTop: 5,
	},
	centeredView: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 22,
	},
	dropDownStyle: {
		backgroundColor: '#fff',
		padding: 8,

		minHeight: 42,

		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',

		borderBottomColor: '#7a7a7a',
		borderBottomWidth: 1,
	},
	errorText: {
		color: 'red',
		fontSize: 12,
		paddingLeft: 5,
	},
});

export default SportsCareerEditScreen;
