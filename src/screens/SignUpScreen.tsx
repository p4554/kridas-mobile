import axios from 'axios';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Platform, ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Divider, Snackbar, Text, TextInput } from 'react-native-paper';
import { FormInputDate } from '../components/forms/FormInputDate';
import FormInputText from '../components/forms/FormInputText';
import envs from '../config/env';
import Logo from '../images/logo.svg';

const SignUpScreen = ({ navigation }: { navigation: any }) => {
	const { BACKEND_URL } = envs;
	const [passwordVisible, setPasswordVisible] = useState(false);
	const [visible, setVisible] = React.useState(false);
	const onToggleSnackBar = () => setVisible(!visible);

	const onDismissSnackBar = () => setVisible(false);

	const [dob, setDob] = useState('');
	const [date, setDate] = useState(new Date());
	const [show, setShow] = useState(false);
	const [mode, setMode] = useState('date');

	const onHandleShowDatePicker = (currentMode: any) => {
		setShow(true);
		setMode(currentMode);
	};

	const datePicker = (event: any, selectedDate: any) => {
		// console.log(event, selectedDate);
		const currentDate = selectedDate || date;
		setShow(Platform.OS === 'ios');
		setDate(currentDate);

		let tempDate = new Date(currentDate);
		let fDate = tempDate.getFullYear() + '-' + (tempDate.getMonth() + 1) + '-' + tempDate.getDate();
		setDob(fDate);
<<<<<<< HEAD
		setValue('dob', tempDate);
=======
		setValue('dob', fDate);
>>>>>>> ccf4d86d45467e75f448934a8c993c629ccb386f
	};

	const {
		setValue,
		control,
		handleSubmit,
		formState: { errors },
	} = useForm();

	const onRegisterPressed = async (data: any) => {
		console.log(data);

		// let payload = {
		// 	first_name: data.firstName,
		// 	last_name: data.lastName,
		// 	user_email: data.email,
		// 	user_dob: data.dob,
		// 	user_type: '{USR}',
		// 	password: data.password,
		// 	registered_referral_code: data.referralCode,
		// };

		var formdata = new FormData();
		formdata.append('first_name', data.firstName);
		formdata.append('last_name', data.lastName);
		formdata.append('user_email', data.email);
		formdata.append('user_dob', data.dob);
		formdata.append('user_type', '{"USR"}');
		formdata.append('password', data.password);
		formdata.append('registered_referral_code', data.referralCode);

		let result = await handleRegister(formdata);
	};

	const handleRegister = async (payload: any) => {
		debugger;
		console.log('dinesh ' + JSON.stringify(payload));
		const config = {
			headers: { 'content-type': 'multipart/form-data' },
		};

		await axios
			.post(`${BACKEND_URL}users/register`, payload, config)
			.then(async (response) => {
				let result = response.data;
				console.log(result);
				navigation.navigate('OtpVerificationScreen', {
					email: result.user_email,
					firstName: result.first_name,
					lastName: result.last_name,
				});
			})
			.catch((error) => {
				console.log(error);
				onToggleSnackBar();

				// error.inner.map((error: any) => {
				// 	console.log('dinesh' + error.message);
				// });

				// setErrors(errorObj);
			});
	};

	return (
		<>
			<ScrollView style={styles.container}>
				<View style={styles.positionLogo}>
					<Logo />
				</View>
				<Text style={styles.signInLabel}>Register Now</Text>
				<View style={styles.input}>
					<View>
						<FormInputText
							name="firstName"
							secureTextEntry={false}
							control={control}
							label="First Name"
							placeHolder="Enter your first name"
							autoComplete="off"
							dense={true}
							autoFocus={true}
							mode="flat"
							onCustomChange={null}
							rules={{
								required: 'First name is required',
							}}
							right={<TextInput.Icon name={'account'} />}
						/>
					</View>

					<View style={styles.elementTopPadding}>
						<FormInputText
							name="lastName"
							secureTextEntry={false}
							control={control}
							label="Last Name"
							placeHolder="Enter your last name"
							autoComplete="off"
							dense={true}
							autoFocus={false}
							mode="flat"
							onCustomChange={null}
							rules={{
								required: 'Last Name is required',
							}}
							right={<TextInput.Icon name={'account'} />}
						/>
					</View>

					<View style={styles.elementTopPadding}>
						<FormInputDate
							name="dob"
							control={control}
							label={'Date of Birth'}
							placeHolder="DOB (yyyy-mm-dd)"
							defaultValue={dob}
							dense={true}
							onCustomChange={datePicker}
							rules={{
								required: 'Date of Birth is required',
							}}
							onHandleShowDatePicker={onHandleShowDatePicker}
							showDatePicker={show}
							dtValue={dob}
							dateMode={mode}
						/>
					</View>

					<View style={styles.elementTopPadding}>
						<FormInputText
							name="email"
							secureTextEntry={false}
							control={control}
							label="Email"
							placeHolder="Enter your email"
							autoComplete="off"
							dense={true}
							autoFocus={false}
							mode="flat"
							onCustomChange={null}
							rules={{
								required: 'Email is required',
								pattern: {
									value: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
									message: 'Please enter a valid email address',
								},
							}}
							right={<TextInput.Icon name={'email-outline'} />}
							keyboardType={'email-address'}
						/>
					</View>
					<View style={styles.elementTopPadding}>
						<FormInputText
							name="password"
							secureTextEntry={passwordVisible}
							control={control}
							label="Password"
							placeHolder="Enter your password"
							autoComplete="off"
							dense={true}
							autoFocus={false}
							mode="flat"
							onCustomChange={null}
							rules={{
								required: 'Password is required',
								minLength: { value: 6, message: 'Password must be at least 6 characters' },
							}}
							right={
								<TextInput.Icon
									name={passwordVisible ? 'eye' : 'eye-off'}
									onPress={() => setPasswordVisible(!passwordVisible)}
								/>
							}
						/>
					</View>
					<View style={styles.elementTopPadding}>
						<FormInputText
							name="referralCode"
							secureTextEntry={false}
							control={control}
							label="Referral Code"
							placeHolder="Referral code if(any)"
							autoComplete="off"
							dense={true}
							autoFocus={false}
							mode="flat"
							onCustomChange={null}
						/>
					</View>
				</View>

				<View style={styles.signUpButton}>
					<TouchableOpacity onPress={handleSubmit(onRegisterPressed)}>
						<View style={styles.buttonOne}>
							<Text style={styles.buttonText}>Register</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View>
					<Text style={styles.registeredInfo}>
						By signing in you are agreeing to our terms and conditions
					</Text>
				</View>
				<Divider style={{ width: '100%' }} />
				<View>
					<Text style={styles.signInText}>Already Registered?</Text>
				</View>
				<View style={styles.signInButton}>
					<TouchableOpacity onPress={() => navigation.navigate('LoginScreen')}>
						<View style={styles.buttonTwo}>
							<Text style={styles.signInButtonText}>Sign In</Text>
						</View>
					</TouchableOpacity>
				</View>
			</ScrollView>

			<Snackbar
				visible={visible}
				onDismiss={onDismissSnackBar}
				theme={{
					colors: {
						onSurface: 'rgb(255,0,0)', // background color
						surface: '#fff', // font color
					},
				}}
			>
				Registration Failed! Possible reasons: Email already registered, Referral code not valid
			</Snackbar>
		</>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: '5%',
	},

	positionLogo: {
		alignItems: 'center',
		padding: 10,
		paddingTop: 20,
	},

	signInLabel: {
		fontWeight: '700',
		fontSize: 20,
		letterSpacing: 1,
		paddingTop: 50,
		paddingBottom: 30,
	},
	input: {
		width: '100%',
		alignSelf: 'center',
	},
	elementTopPadding: {
		paddingTop: 20,
	},
	signUpButton: {
		paddingTop: 30,

		alignSelf: 'center',
		width: '100%',
	},

	buttonOne: {
		borderRadius: 2,
		paddingVertical: 14,

		backgroundColor: '#0062BD',
	},

	buttonText: {
		color: 'white',
		fontSize: 18,
		lineHeight: 24,
		textAlign: 'center',
	},
	registeredInfo: {
		fontSize: 10,
		color: '#AAAAAA',

		paddingBottom: 20,

		paddingTop: 15,
		alignSelf: 'center',
	},
	signInText: {
		fontWeight: '400',
		fontSize: 18,
		paddingTop: 30,
		alignSelf: 'center',
	},
	signInButton: {
		paddingTop: 24,
		alignSelf: 'center',
		width: '100%',
	},
	buttonTwo: {
		borderRadius: 2,
		borderWidth: 2,
		borderColor: '#0062BD',
		paddingVertical: 14,

		backgroundColor: 'white',
	},
	signInButtonText: {
		fontWeight: '400',
		fontSize: 18,
		alignSelf: 'center',
		color: '#2F80ED',
	},
	textInput: {
		display: 'flex',
		flexDirection: 'row',
	},
	inputStyle: {
		borderBottomColor: '#0062BD',
		borderBottomWidth: 2,
	},
});

export default SignUpScreen;
