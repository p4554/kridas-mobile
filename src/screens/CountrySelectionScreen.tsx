import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Image, Pressable, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import CountryPicker from '../components/CountryPicker';
import StatePicker from '../components/StatePicker';
import envs from '../config/env';
import imagesPath from '../constants/imagesPath';
import UserIcon from '../images/blackusercircle.svg';
import SportsLogo from '../images/sportslogo.svg';
import { Country } from './../types';

const CountrySelectionScreen = ({ navigation }: { navigation: any }) => {
	const { BACKEND_URL } = envs;
	const [user, setUser] = useState<any>('');

	const [selectedCountry, setSelectedCountry] = useState<any>(null);
	const [selectedState, setSelectedState] = useState<any>(null);

	const [state, setState] = useState('');

	const [countryArray, setCountryArray] = useState<any>([]);

	const [country, setCountry] = useState<Country>();

	const [openModal, setOpenModal] = useState(false);

	const [openDropDown, setOpenDropDown] = useState(false);

	const [countryStates, setCountryStates] = useState([]);
	const [stateOpenModal, setStateOpenModal] = useState(false);

	// errors
	const [errCountryState, setErrCountryState] = useState('');
	const [errCountry, setErrCountry] = useState('');

	const {
		control,
		handleSubmit,
		formState: { errors },
	} = useForm();

	useEffect(() => {
		(async function () {
			const value = await AsyncStorage.getItem('currentUser');
			const userValue = JSON.parse(value || '{}');
			setUser(userValue);

			getCountryArray();
		})();
		setErrCountryState('');
		setErrCountry('');
	}, []);

	const getCountryArray = async () => {
		const response = await fetch(`${BACKEND_URL}/country/getAll`);
		const result = await response.json();

		let country_array = result.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each sport

		setCountryArray(country_array);
		//console.log(JSON.stringify(countryArray));
	};

	const updateValue = async () => {
		console.log('updateCountryValue', selectedCountry?.country_code);
		console.log('updateStateValue', selectedState?.state_code);

		if (selectedCountry === null && selectedState === null) {
			setErrCountry('Please select country');
			setErrCountryState('Please select state');
			return false;
		} else if (selectedCountry === null) {
			setErrCountry('Please select country');
			return false;
		} else if (selectedState === null) {
			setErrCountryState('Please select state');
			return false;
		}

		// // const s =  JSON.stringify(user.address)
		// console.log('fffff===>', userVal.data.sports_interested)
		const userType = user.user_type;
		const userTypeValue = JSON.stringify(userType);
		const sportsInterest = user.sports_interested;
		const sportsInterestValue = JSON.stringify(sportsInterest);
		let countryValue = selectedCountry?.country_code;
		// console.log(sportsValue)
		setCountry(countryValue);
		let stateValue = selectedState?.state_code;
		setState(stateValue);
		let obj = {
			country: countryValue,
			state: stateValue,
		};

		let details = JSON.stringify(obj);
		console.log("details", details)

		var formData = new FormData();
		formData.append('first_name', user.first_name);
		formData.append('last_name', user.last_name);
		formData.append('user_email', user.user_email);
		formData.append('user_type', userTypeValue);
		formData.append('user_id', user.user_id);
		formData.append('user_dob', user.user_dob);
		formData.append('sports_interested', sportsInterestValue);
		formData.append('address', details);

		let requestOptions = {
			method: 'PUT',
			headers: {
				Accept: 'application/json',
			},
			body: formData,
		};

		fetch(`${BACKEND_URL}users`, requestOptions)
			.then((response) => response.json())
			.then(async (result) => {
				console.log("address",result);
				const userValue = JSON.stringify(result);
				await AsyncStorage.setItem('currentUser', userValue);
				if (result) {
					navigation.navigate('Tab');
				} else {
					alert('! Please fill all the required fields');
				}
			})
			.catch((error) => console.log('error', error));
	};

	const onPress = () => {
		setOpenDropDown(false);
	};

	const handleSelectedCountry = (value: any) => {
		//re - initialize the variables
		setErrCountryState('');
		setErrCountry('');
		setCountryStates([]);
		setSelectedCountry(value);

		setOpenModal(false);

		// reset all checked flag to false
		let country_array = countryArray.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each element

		// compare selected country with each element and set checked to true
		setCountryArray(
			country_array.map((item: { country_id: any; checked: boolean }) =>
				item.country_id === value.country_id ? { ...item, checked: !item.checked } : item
			)
		);

		let states = value.country_states;
		setCountryStates(states.length > 0 ? states : []);
		console.log("s", countryStates)
		setSelectedState(null);
	};

	const handleSelectedState = (value: any) => {
		setSelectedState(value);
		setStateOpenModal(false);
	};

	const handleStateClicked = () => {
		if (countryStates.length > 0) {
			setErrCountryState('');
			setStateOpenModal(!stateOpenModal);
		} else {
			setErrCountryState('Please select country first');
		}
	};

	return (
		<ScrollView>
			<Pressable onPress={onPress}>
				<View style={styles.container}>
					<View style={styles.positionSportsLogo}>
						<SportsLogo />
					</View>
					<UserIcon />
					<Text style={styles.title}>Step 2 of 2 {'\n'}Select your location</Text>
					<Text style={styles.content}>Your city and country.</Text>
					<View style={styles.centeredView}></View>
					<TouchableOpacity
						style={styles.dropDownStyle}
						activeOpacity={0.8}
						onPress={() => setOpenModal(!openModal)}
					>
						<Text> {!!selectedCountry ? selectedCountry?.country_name : 'Choose a Country'}</Text>
						<Image source={imagesPath.icDropDown} />
					</TouchableOpacity>
					{errCountry && errCountry !== '' ? (
						<View>
							<Text style={styles.errorText}>{errCountry}</Text>
						</View>
					) : null}

					<View style={{ paddingTop: 24 }}>
						<TouchableOpacity
							style={{ ...styles.dropDownStyle }}
							activeOpacity={0.8}
							onPress={handleStateClicked}
						>
							<Text> {!!selectedState ? selectedState?.state_name : 'Choose a State'}</Text>
							<Image source={imagesPath.icDropDown} />
						</TouchableOpacity>
					</View>
					{errCountryState && errCountryState !== '' ? (
						<View>
							<Text style={styles.errorText}>{errCountryState}</Text>
						</View>
					) : null}

					<CountryPicker
						setOpenModal={setOpenModal}
						openModal={openModal}
						value={selectedCountry}
						setValue={handleSelectedCountry}
						items={countryArray}
						displayElement={'country_name'}
					/>
					{countryStates?.length > 0 && (
						<StatePicker
							setStateOpenModal={setStateOpenModal}
							stateOpenModal={stateOpenModal}
							stateValue={selectedState}
							setStateValue={handleSelectedState}
							items={countryStates}
							displayElement={'state_name'}
						/>
					)}

					<View style={styles.button}>
						<TouchableOpacity onPress={updateValue}>
							<View style={styles.buttonStyle}>
								<Text style={styles.buttonText}>Next</Text>
							</View>
						</TouchableOpacity>
					</View>
				</View>
			</Pressable>
		</ScrollView>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: '5%',
	},
	positionSportsLogo: {
		alignItems: 'center',
		padding: 10,
		paddingTop: 50,
	},
	title: {
		paddingTop: 18,
		fontWeight: '700',
		fontSize: 18,
	},

	content: {
		paddingTop: 18,
		fontWeight: '400',
		fontSize: 16,
		paddingBottom: 10,
	},
	input: {
		display: 'flex',
		// flexDirection: 'row',
		// flexWrap: 'wrap',
		flex: 1,
	},
	checkbox: {
		paddingTop: 18,
		marginBottom: 20,
	},
	checkboxText: {
		fontSize: 18,
		lineHeight: 24,
		// paddingTop: 16,
		marginLeft: 4,
	},
	section: {
		display: 'flex',
		flexDirection: 'row',
	},
	button: {
		paddingTop: 20,
		// marginLeft: Dimensions.get('window').width / 6
		alignSelf: 'center',
		width: '100%',
	},

	buttonStyle: {
		borderRadius: 2,
		paddingVertical: 14,
		// paddingHorizontal: 10,
		backgroundColor: '#0062BD',
		// width: "80%"
	},

	buttonText: {
		color: 'white',
		fontSize: 18,
		lineHeight: 24,
		textAlign: 'center',
	},
	elementTopPadding: {
		paddingTop: 20,
	},
	centeredView: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 22,
	},
	dropDownStyle: {
		backgroundColor: '#fff',
		padding: 8,

		minHeight: 42,

		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',

		borderBottomColor: '#7a7a7a',
		borderBottomWidth: 1,
	},
	errorText: {
		color: 'red',
		fontSize: 12,
		paddingLeft: 5,
	},
});

export default CountrySelectionScreen;

{
	/* <CustomPicker
						setOpenFSModel={setOpenFSModal}
						openFSModal={openFSModal}
						value={selectedCountry}
						setValue={setSelectedCountry}
						items={countryArray}
					/> */
}
