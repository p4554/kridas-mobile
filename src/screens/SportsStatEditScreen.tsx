import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react';
import { useForm, useFieldArray } from 'react-hook-form';
import { Image, Modal, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Divider, RadioButton } from 'react-native-paper';
import { FormInputText } from '../components/forms/FormInputText';
import SportsPicker from '../components/SportsPicker';
import SportsSkillPicker from '../components/SportsSkillPicker';
import envs from '../config/env';
import imagesPath from '../constants/imagesPath';
import Back from '../images/back.svg';
import DynamicForm from '../components/DynamicForm';

const SportsStatEditScreen = ({ modalOpen, setModalOpen, userProfile, statisticsId, statistics }: any) => {
	const { BACKEND_URL } = envs;
	const statId = statisticsId
	console.log("id", statId);
	// const [user, setUser] = useState('');

	const [checked, setChecked] = React.useState('');

	const [selectedSports, setSelectedSports] = useState<any>(null);

	const [selectedItem, setSelectedItem] = useState<any>(null);

	const [sportsArray, setSportsArray] = useState<any>([]);

	const [sports, setSports] = useState<any>(null);

	const [openModal, setOpenModal] = useState(false);

	const [openDropDown, setOpenDropDown] = useState(false);

	const [errSports, setErrSports] = useState('');

	const [selectedSkill, setSelectedSkill] = useState<any>(null);

	const [skillArray, setSkillArray] = useState<any>([]);

	const [skill, setSkill] = useState<any>(null);

	const [skillOpenModal, setSkillOpenModal] = useState(false);

	const [skillOpenDropDown, setSkillOpenDropDown] = useState(false);

	const [errSkill, setErrSkill] = useState('');

	const {
		setValue,
		control,
		handleSubmit,
		formState: { errors },
		register
	} = useForm();

	//For Dynamic Form
    const { fields, append, remove } = useFieldArray({
        control, // control props comes from useForm (optional: if you are using FormContext)
        name: "url", // unique name for your Field Array
      });

	useEffect(() => {
		(async function () {
			const value = await AsyncStorage.getItem('currentUser');

			getSportsArray();
			getSkillArray();
		})();

		setErrSports('');
		setErrSkill('');

		setValue('sports', statistics?.user_gender);
	}, []);

	const getSportsArray = async () => {
		const response = await fetch(`${BACKEND_URL}/sports/getAll`);
		const result = await response.json();
		// console.log("r", result);

		let sports_array = result.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each sport

		setSportsArray(sports_array);
		// console.log(JSON.stringify(sportsArray));
	};

	const handleSelectedSports = (value: any) => {
		//re - initialize the variables
		setErrSports('');
		setSelectedSports(value);

		setOpenModal(false);

		// reset all checked flag to false
		let sports_array = sportsArray.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each element

		// compare selected country with each element and set checked to true
		setSportsArray(
			sports_array.map((item: { sports_id: any; checked: boolean }) =>
				item.sports_id === value.sports_id ? { ...item, checked: !item.checked } : item
			)
		);
	};

	const getSkillArray = async () => {
		const response = await fetch(`${BACKEND_URL}/category/getByParentType/SKI`);
		const result = await response.json();
		const resultValue = result.data
		// console.log("r", resultValue);

		let skill_array = resultValue.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each category

		setSkillArray(skill_array);
		// console.log(JSON.stringify(skillArray));
	};

	const handleSelectedSkill = (skillValue: any) => {
		//re - initialize the variables
		setErrSkill('');
		setSelectedSkill(skillValue);

		setSkillOpenModal(false);

		// reset all checked flag to false
		let skill_array = skillArray.map((item: any) => {
			return { ...item, checked: false };
		}); // set checked to false in each element

		// compare selected skill with each element and set checked to true
		setSkillArray(
			skill_array.map((item: { category_id: any; checked: boolean }) =>
				item.category_id === skillValue.category_id ? { ...item, checked: !item.checked } : item
			)
		);
	};

	const updateValue = async (data: any) => {
		// console.log('updateSports', selectedSports?.sports_id);
		console.log('updateSkill', selectedSkill?.category_id);
		console.log("checked", checked);
		console.log("url", data.url);

		const sports_id = statId.map((sportsID: any) => {
			return sportsID?.sports_id
		})
		const sportsId = sports_id[0]
		const stat_id = statId.map((statId: any) => {
			return statId?.stat_id
		})
		const statisticsId = stat_id[0]

		// console.log("1", sportsId)
		// console.log("2", statisticsId)

		// if ( selectedSkill === null) {
		// 	setErrSkill('Please select Skill');
		// 	return false;
		// }

		// let countryValue = selectedCountry?.country_code;
		// console.log(sportsValue)
		// setCountry(countryValue);
		let skill = selectedSkill?.category_id;
		setSkill(skill);
		// let obj = {
		// 	country: countryValue,
		// 	state: stateValue,
		// };

		// let details = JSON.stringify(obj);
		// console.log("details", details)
		const array = [data.url]
		// let a = array.push(data.url)
		const details = JSON.stringify(array)

		var formData = new FormData();
		formData.append('user_statistics_id', statisticsId);
		formData.append('user_id', userProfile.user_id);
		formData.append('sports_id', sportsId);
		formData.append('skill_level', skill);
		formData.append('playing_status', checked);
		formData.append('statistics_links',details );

		let requestOptions = {
			method: 'PUT',
			headers: {
				Accept: 'application/json',
			},
			body: formData,
		};

		fetch(`${BACKEND_URL}/users/statistics`, requestOptions)
			.then((response) => response.json())
			.then(async (result) => {
				console.log("address",result);
				setModalOpen(false);
			})
			.catch((error) => console.log('error', error));
	};


	return (
		<>
			<Modal
				animationType="slide"
				transparent={false}
				visible={modalOpen}
				onRequestClose={() => {
					//	setModalOpen(!modalOpen);
				}}
			>
				<View style={styles.header}>
					<View>
						<TouchableOpacity style={styles.headerContent} onPress={() => setModalOpen(!modalOpen)}>
							<Back style={styles.backIcon} />
							<Text style={styles.headerText}>Edit Interests</Text>
						</TouchableOpacity>
					</View>
					<View>
						<TouchableOpacity onPress={handleSubmit(updateValue)}>
							<Text style={styles.buttonText}>Save</Text>
						</TouchableOpacity>
					</View>
				</View>
				<Divider />
				<ScrollView>
					<View style={styles.container}>
						<Text style={styles.title}>SPORTS YOU PLAY & SCORES</Text>
						<View style={styles.input}>
							<View>
								<View style={styles.centeredView}></View>
								<TouchableOpacity
									style={styles.dropDownStyle}
									activeOpacity={0.8}
									onPress={() => setOpenModal(!openModal)}
								>
									<Text> {!!selectedSports ? selectedSports?.sports_name : 'Choose a Sports'}</Text>
									<Image source={imagesPath.icDropDown} />
								</TouchableOpacity>
								{errSports && errSports !== '' ? (
									<View>
										<Text style={styles.errorText}>{errSports}</Text>
									</View>
								) : null}
								<SportsPicker
									setOpenModal={setOpenModal}
									openModal={openModal}
									value={selectedSports}
									setValue={handleSelectedSports}
									items={sportsArray}
									displayElement={'sports_name'}
								/>
							</View>
							<View style={styles.elementTopPadding}>
								<View style={styles.centeredView}></View>
								<TouchableOpacity
									style={styles.dropDownStyle}
									activeOpacity={0.8}
									onPress={() => setSkillOpenModal(!skillOpenModal)}
								>
									<Text> {!!selectedSkill ? selectedSkill?.category_name : 'Choose a Skill'}</Text>
									<Image source={imagesPath.icDropDown} />
								</TouchableOpacity>
								{errSkill && errSkill !== '' ? (
									<View>
										<Text style={styles.errorText}>{errSkill}</Text>
									</View>
								) : null}
								<SportsSkillPicker
									setSkillOpenModal={setSkillOpenModal}
									skillOpenModal={skillOpenModal}
									skillValue={selectedSkill}
									setSkillValue={handleSelectedSkill}
									items={skillArray}
									displayElement={'category_name'}
									/>
							</View>
							<View style={styles.elementTopPadding}>
								<View style={styles.radioButton}>
									<RadioButton
										testID='Active'
										value="AC"
										status={checked === 'AC' ? 'checked' : 'unchecked'}
										onPress={() => setChecked('AC')}
									/>
									<Text style={styles.sportsInfo}>I am actively playing</Text>
								</View>
							</View>
							<View style={styles.elementTopPadding}>
								<View style={styles.radioButton}>
									<RadioButton
										testID='Inactive'
										value="IC"
										status={checked === 'IC' ? 'checked' : 'unchecked'}
										onPress={() => setChecked('IC')}
									/>
									<Text style={styles.sportsInfo}>I am not actively playing</Text>
								</View>
							</View>
							<View style={styles.elementTopPadding}>
								<Text style={styles.subTitle}>External Stats URL (optional)</Text>
							</View>
							<View style={styles.elementTopPadding}>
								{/* {fields.map((item, index) => (
									<View key={item.id}>
										<FormInputText
										name={`links.${index}.value`}
										secureTextEntry={false}
										control={control}
										label="URL"
										placeHolder="URL"
										autoComplete="off"
										dense={true}
										autoFocus={false}
										mode="flat"
										onCustomChange={null}
										/>
										<TouchableOpacity onPress={() => remove(index)}>
											<Text>Remove</Text>
										</TouchableOpacity>
									</View>
									
								))}
								<TouchableOpacity onPress={() => append({ value: "" })}>
									<Text>Add</Text>
								</TouchableOpacity> */}
								{/* <FormInputText
									name="url"
									secureTextEntry={false}
									control={control}
									label="URL"
									placeHolder="URL"
									autoComplete="off"
									dense={true}
									autoFocus={false}
									mode="flat"
									onCustomChange={null}
								/> */}
								{/* FOr Dynamic Form */}
								{fields.map(({field, index}: any) => {
									return (
										<>
										<View key={field.id}>
										<FormInputText
											{...register(`url.${index}.name` as const, {
												required: true
											})}
											name="url"
											secureTextEntry={false}
											control={control}
											label="URL"
											placeHolder="URL"
											autoComplete="off"
											dense={true}
											autoFocus={false}
											mode="flat"
											onCustomChange={null}
										/>
										<TouchableOpacity onPress={() => remove(index)}>
											<Text>Remove</Text>
										</TouchableOpacity>
										</View>
										<TouchableOpacity onPress={() => append({})}>
											<Text>Add another</Text>
										</TouchableOpacity>
										</>

										
									)
								})}
							</View>
							<View style={styles.elementTopPadding}>
								<Text style={styles.add}>+ Add another</Text>
							</View>
							<View style={styles.elementTopPadding}>
								<Text style={styles.subTitle}>Upload Certificates / Documents / Proofs (optional)</Text>
							</View>
							<View>
								<TouchableOpacity>
									<View>
										<Text style={styles.buttonText}>Upload</Text>
									</View>
								</TouchableOpacity>
							</View>
							<View style={styles.elementTopPadding}>
								<Text style={styles.add}>+ Add another</Text>
							</View>
							<View style={styles.elementTopPadding}>
								<Text style={styles.add}>+ Add another Org/Club</Text>
							</View>
						</View>
					</View>
				</ScrollView>
			</Modal>
		</>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: '5%',
	},
	title: {
		fontSize: 16,
		fontWeight: 'bold',
		letterSpacing: 3.5,
		color: '#949494',
	},
	sportsInfo: {
		fontSize: 16,
		paddingTop: 5,
	},
	input: {
		width: '100%',
		alignSelf: 'center',
	},
	elementTopPadding: {
		paddingTop: 20,
	},
	subTitle: {
		fontSize: 16,
		fontWeight: '700',
		color: '#555555',
	},
	add: {
		fontSize: 16,
		color: '#2F80ED',
	},
	radioButton: {
		display: 'flex',
		flexDirection: 'row',
	},
	headerContent: {
		display: 'flex',
		flexDirection: 'row',
	},
	headerText: {
		fontSize: 16,
		paddingLeft: 10,
	},
	backIcon: {
		paddingTop: 23,
	},
	header: {
		padding: '5%',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	buttonText: {
		fontSize: 16,
		color: '#2F80ED',
	},
	centeredView: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 22,
	},
	dropDownStyle: {
		backgroundColor: '#fff',
		padding: 8,

		minHeight: 42,

		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',

		borderBottomColor: '#7a7a7a',
		borderBottomWidth: 1,
	},
	errorText: {
		color: 'red',
		fontSize: 12,
		paddingLeft: 5,
	},
});

export default SportsStatEditScreen;
