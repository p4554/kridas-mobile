import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Checkbox, Snackbar, Text } from 'react-native-paper';
import envs from '../config/env';
import UserIcon from '../images/blackusercircle.svg';
import SportsLogo from '../images/sportslogo.svg';

const SportsSelectionScreen = ({ navigation }: { navigation: any }) => {
	const { BACKEND_URL } = envs;
	const [user, setUser] = useState<any>();
	const [visible, setVisible] = React.useState(false);
	const onToggleSnackBar = () => setVisible(!visible);

	const onDismissSnackBar = () => setVisible(false);

	const [interestedSports, setInterestedSports] = useState<any>();

	useEffect(() => {
		(async function () {
			const value = await AsyncStorage.getItem('currentUser');
			const userValue = JSON.parse(value || '{}');
			setUser(userValue);
		})();
	}, []);

	// Get all possible interest sports & set in state (sports)
	useEffect(() => {
		axios
			.get(`${BACKEND_URL}sports/getAll`)
			.then((result: any) => {
				let sports_array = result.data.map((item: any) => {
					return { ...item, checked: false };
				}); // set checked to false in each sport

				setInterestedSports(sports_array);
			})
			.catch((error: any) => {
				console.log(error);
			});
	}, []);

	const toggleCheckbox = (id: any) => {
		setInterestedSports(
			interestedSports.map((item: { sports_id: any; checked: boolean }) =>
				item.sports_id === id ? { ...item, checked: !item.checked } : item
			)
		);
	};

	const getSelectedSports = async () => {
		var ids = interestedSports.map((i: any) => i?.sports_id);
		var checked = interestedSports.map((c: any) => c?.checked);
		let selected = [];
		for (let i = 0; i < checked.length; i++) {
			if (checked[i] == true) {
				selected.push(ids[i]);
			}
		}

		if (selected.length < 3) {
			onToggleSnackBar();
			return true;
		}

		let value = JSON.stringify(selected);
		console.log('fgfgfg', value);

		console.log('user.first_name', user.first_name);

		var formData = new FormData();
		formData.append('first_name', user.first_name);
		formData.append('last_name', user.last_name);
		formData.append('user_email', user.user_email);
		formData.append('user_type', 'USR');
		formData.append('user_dob', user.user_dob);
		formData.append('user_id', user.user_id);
		formData.append('referral_code', user.referral_code);
		formData.append('user_name', user.user_name);
		formData.append('user_profile_verified', user.user_profile_verified);
		formData.append('user_status', user.user_status);
		formData.append('user_phone', user.user_phone);
		formData.append('address', user.address);
		formData.append('bio_details', user.bio_details);
		formData.append('social', user.social);
		formData.append('sports_interested', value);

		let requestOptions = {
			method: 'PUT',
			headers: {
				Accept: 'application/json',
				// "Content-Type": "application/json",
			},
			body: formData,
		};

		const config = {
			headers: { 'content-type': 'application/json' },
		};

		// axios
		// 	.put('http://3.143.119.220:5001/api/users', formData, config)
		// 	.then(async (result) => {
		// 		console.log(result);
		// 		const userValue = JSON.stringify(result);
		// 		await AsyncStorage.setItem('currentUser', userValue);
		// 		if (result) {
		// 			navigation.navigate('CountrySelectionScreen');
		// 		} else {
		// 			alert('! Please select atleast one Sports');
		// 		}
		// 	})
		// 	.catch((error) => console.log('error', error.message));

		fetch(`${BACKEND_URL}/users`, requestOptions)
			.then((response) => response.json())
			.then(async (result) => {
				console.log(result);
				const userValue = JSON.stringify(result);
				await AsyncStorage.setItem('currentUser', userValue);
				if (result) {
					navigation.navigate('CountrySelectionScreen');
				}
			})
			.catch((error) => console.log('error', error));
	};

	return (
		<>
			<ScrollView>
				<View style={styles.container}>
					<View style={styles.positionSportsLogo}>
						<SportsLogo />
					</View>
					<UserIcon />
					<Text style={styles.title}>Step 1 of 2 {'\n'}Select atleast 3 interests</Text>
					<Text style={styles.content}>Select the sports you play and/or follow.</Text>
					<View style={styles.input}>
						{interestedSports?.map((item: any) => (
							<View key={item.sports_id}>
								<View style={styles.section} key={item.sports_id}>
									<Checkbox
										status={item?.checked === true ? 'checked' : 'unchecked'}
										onPress={() => {
											toggleCheckbox(item.sports_id);
										}}
										color={item?.checked ? '#4630EB' : '#0062BD'}
									/>
									<Text onPress={() => toggleCheckbox(item.sports_id)} style={styles.label}>
										{item.sports_name}
									</Text>
								</View>
							</View>
						))}
					</View>
				</View>
			</ScrollView>
			<View style={styles.button}>
				<TouchableOpacity onPress={getSelectedSports}>
					<View style={styles.buttonStyle}>
						<Text style={styles.buttonText}>Next</Text>
					</View>
				</TouchableOpacity>
			</View>
			<Snackbar
				visible={visible}
				onDismiss={onDismissSnackBar}
				duration={2000}
				theme={{
					colors: {
						onSurface: 'rgb(255,0,0)', // background color
						surface: '#fff', // font color
					},
				}}
			>
				Select atleast three sports
			</Snackbar>
		</>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: '5%',
	},
	positionSportsLogo: {
		alignItems: 'center',
		padding: 10,
		paddingTop: 50,
	},
	title: {
		paddingTop: 18,
		fontWeight: '700',
		fontSize: 18,
	},

	content: {
		paddingTop: 18,
		fontWeight: '400',
		fontSize: 16,
		paddingBottom: 10,
	},
	input: {
		display: 'flex',
		// flexDirection: 'row',
		// flexWrap: 'wrap',
		flex: 1,
	},
	checkbox: {
		paddingTop: 18,
		marginBottom: 20,
	},
	label: {
		fontSize: 18,

		paddingTop: 6,
		//marginLeft: 4,
	},
	section: {
		display: 'flex',
		flexDirection: 'row',
		paddingBottom: 2,
	},
	button: {
		paddingTop: 30,
		// marginLeft: Dimensions.get('window').width / 6
		alignSelf: 'center',
		width: '100%',
	},

	buttonStyle: {
		borderRadius: 2,
		paddingVertical: 14,
		// paddingHorizontal: 10,
		backgroundColor: '#0062BD',
		// width: "80%"
	},

	buttonText: {
		color: 'white',
		fontSize: 18,
		lineHeight: 24,
		textAlign: 'center',
	},
});

export default SportsSelectionScreen;
