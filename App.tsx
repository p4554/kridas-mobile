import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer, Theme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import { QueryClient, QueryClientProvider } from 'react-query';
import useCachedResources from './src/hooks/useCachedResources';
import useColorScheme from './src/hooks/useColorScheme';
import TabNavigation from './src/navigation/TabNavigation';
import CountrySelectionScreen from './src/screens/CountrySelectionScreen';
import LoginScreen from './src/screens/LoginScreen';
import ModalScreen from './src/screens/ModalScreen';
import OtpVerificationScreen from './src/screens/OtpVerificationScreen';
import SignUpScreen from './src/screens/SignUpScreen';
import SportsSelectionScreen from './src/screens/SportsSelectionScreen';
import SuccessScreen from './src/screens/SuccessScreen';
import { combineThemes } from './src/theme';

const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();
const queryClient = new QueryClient();

export default function App() {
	const isLoadingComplete = useCachedResources();
	const colorScheme = useColorScheme();
	const theme = combineThemes(colorScheme);

	if (!isLoadingComplete) {
		return null;
	} else {
		return (
			<PaperProvider theme={theme as ReactNativePaper.Theme}>
				<NavigationContainer theme={theme as Theme} independent={true}>
					<QueryClientProvider client={queryClient}>
						<Stack.Navigator initialRouteName="LoginScreen">
							<Stack.Screen name="LoginScreen" component={LoginScreen} options={{ headerShown: false }} />
							<Stack.Screen
								name="SignUpScreen"
								component={SignUpScreen}
								options={{ presentation: 'modal' }}
							/>
							<Stack.Screen
								name="OtpVerificationScreen"
								component={OtpVerificationScreen}
								options={{ headerShown: false }}
							/>
							<Stack.Screen
								name="SuccessScreen"
								component={SuccessScreen}
								options={{ headerShown: false }}
							/>
							<Stack.Screen
								name="SportsSelectionScreen"
								component={SportsSelectionScreen}
								options={{ headerShown: false }}
							/>
							<Stack.Screen
								name="CountrySelectionScreen"
								component={CountrySelectionScreen}
								options={{ headerShown: false }}
							/>
							<Stack.Screen name="Tab" component={TabNavigation} options={{ headerShown: false }} />
							<Stack.Screen name="Modal" component={ModalScreen} />
						</Stack.Navigator>
						{/* <Drawer.Navigator
						useLegacyImplementation={true}
						screenOptions={{ headerShown: true, headerLeft: () => <MenuIcon /> }}
						drawerContent={(props) => <MenuContent {...props} />}
					>
						<Drawer.Screen name="My Friends" component={MyFriends} />
						<Drawer.Screen name="Profile" component={Profile} />
					</Drawer.Navigator> */}
					</QueryClientProvider>
				</NavigationContainer>
			</PaperProvider>
		);
	}
}
